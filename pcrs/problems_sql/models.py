from django.conf import settings
from django.db import models
from django.db.models.signals import post_delete

from pcrs.settings import SQAM_USED
from problems.models import (AbstractSubmission, AbstractTestRun,
    SubmissionPreprocessorMixin,
    testcase_delete, problem_delete)
from problems_rdb.db_wrapper import StudentWrapper
from problems_rdb.models import RDBProblem, RDBTestCase

class Problem(RDBProblem):
    """
    A SQL problem, extends RDBProblem.

    The problem solution is expected to be a set of valid SQL expressions.
    The value in order_matters defines whether the solution comparison
    is exact, or set-based.

    When a SQLProblem is deleted, any associated SQLTestCases are also deleted.
    """
    language = 'sql'
    order_matters = models.BooleanField(null=False, default=False)

    @property
    def affect_submissions(self):
        return ['solution', 'order_matters']

    def validate_solution(self):
        self._run_solution(self.solution)


class Submission(SubmissionPreprocessorMixin, AbstractSubmission):
    problem = models.ForeignKey(Problem, on_delete=models.CASCADE)
    
    def run_testcases(self, request, save=True):
        results = []
        testcases = self.problem.testcase_set.all()

        submittedFiles = self.preprocessTags()
        # We don't support multiple files yet
        submittedCode = submittedFiles[0]['code']

        if self.problem.pk == 9999999:                # Editor
            save = False
            # Allows get_results to run with the same results on both sides
            self.problem.solution = submittedCode
            # TODO: Setting a specific dataset that matches the schema
            testcases = [TestCase.objects.get(pk=11)]

        with StudentWrapper(database=settings.RDB_DATABASE,
                            user=request.user.username) as db:
    
            for testcase in testcases:
                dataset = testcase.dataset
                if dataset.using_sqam and SQAM_USED:
                    sqam_result = db.run_sqam_testcase(self.problem.solution, submittedCode,
                                            dataset.namespace)
                    result = sqam_result
                    result['sqam'] = True
                    # syntax error
                    if result['error'] is not None:
                        result['error_desc'] = result['error']
                        result['error'] = True
                        mark = 0
                        
                    else:
                        mark = sqam_result['mark']
                        result['passed'] = mark >= 50
                        result['error'] = False
                        if result['hasErrors']:
                            result['missingRows'] = [list(row) for row in result['missingRows']]
                            result['extraRows'] = [list(row) for row in result['extraRows']]
                        
                    result['similarity'] = mark
                    result['score'] = round((mark/100) * dataset.mark, 2)
                    result['max_score'] = dataset.mark
                else:
                    result = db.run_testcase(self.problem.solution, submittedCode,
                                            dataset.namespace,
                                            self.problem.order_matters)
                    result['sqam'] = False
                if save:
                    TestRun(submission=self, testcase=testcase,
                            test_passed=result['passed']).save()

                result['testcase'] = testcase.id
                result['test_desc'] = str(testcase)
                result['visible'] = testcase.is_visible
                # checking attempts for unicorn reward
                result['numAttempts'] = Submission.objects.filter(problem = self.problem, user = self.user).count()
                results.append(result)
        return results, None

class TestCase(RDBTestCase):
    """
    A test case for a SQL problem. A SQLTestCase consists of an associated
    SQLProblem and a Dataset.

    When a Dataset or a SQLProblem is deleted, any associated SQLTestCases
    are also deleted.
    """
    problem = models.ForeignKey(Problem, on_delete=models.CASCADE)



class TestRun(AbstractTestRun):
    testcase = models.ForeignKey(TestCase, on_delete=models.CASCADE)
    submission = models.ForeignKey(Submission, on_delete=models.CASCADE)

    def get_history(self):
        return {
            'visible': False,
            'input': '',
            'output': '',
            'passed': self.test_passed,
            'description': str(self.testcase)
        }

# update submission scores when a testcase is deleted
post_delete.connect(testcase_delete, sender=TestCase)

post_delete.connect(problem_delete, sender=Problem)
