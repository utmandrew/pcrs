import json
import problems.pcrs_languages as languages
import languages.riscv.riscv_server as riscv_server


class RiscVSpecifics(languages.BaseLanguage):
    # See python equivalent

    # Externally used
    def encode_str(self, target_value):
        raise NotImplementedError(
            "[RiscVSpecifics] encode_str not implemented")

    def get_exec_trace(self, user_script, add_params=None) -> str:
        with riscv_server.new_riscv_executor_server() as server:
            cm = server.new_code_model(user_script)
            status = cm.run()
            if status == cm.SUCCESS:
                return cm.get_trace_as_json()
            elif status == cm.FAILURE:
                return json.dumps({'traceError': cm.get_error_message()})

    # Externally used (also see comment in pcrs_languages.py about refactor?)
    def run_test(self, user_script, test_input, exp_output, pre_code=""):
        ret = ""
        user_script = str(user_script)
        test_input = str(test_input)
        exp_output = str(exp_output)
        pre_code = str(pre_code)
        my_test_input = test_input
        if "output" in pre_code:
            my_test_input = pre_code[7::]
        elif "register" in pre_code:
            my_test_input = pre_code[9::]

        exp_output = exp_output.strip()
        if test_input == '""':
            my_test_input = ""
        exp_output = exp_output.replace("\r", "")
        user_script = user_script.replace("\r", "")
        try:
            with riscv_server.new_riscv_executor_server() as server:

                model = server.new_code_model(user_script,my_test_input)                
                status = model.run()
                
                if status == riscv_server.RiscVCodeModel.FAILURE:
                    ret = {'passed_test': 1==2,
                    'expected_output': exp_output, 'test_val': model.get_error_message().splitlines()[-1]}
                    
                elif("output" in pre_code):
                    check = model.getoutput().strip()

                    ret = {'passed_test': check == exp_output,
                    'expected_output': json.dumps(exp_output), 'test_val': json.dumps(check)}
                else:               
                    ret = {'passed_test': model.getregistervalue(test_input)==int(exp_output),
                    'expected_output': exp_output, 'test_val': str(model.getregistervalue(test_input))}
        except Exception as e:
            print(e)
        return ret

     # Externally used
    def get_download_mimetype(self):
        raise NotImplementedError(
            "[RiscVSpecifics] get_download_mimetype not implemented")
