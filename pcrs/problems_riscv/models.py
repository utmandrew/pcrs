from django.db import models
from django.core.exceptions import ValidationError

from problems.models import AbstractProgrammingProblem, AbstractSubmission, AbstractTestCaseWithDescription, AbstractTestRun, SubmissionPreprocessorMixin, problem_delete, testcase_delete
from pcrs.model_helpers import has_changed
from problems.pcrs_languages import GenericLanguage
from django.db.models.signals import post_delete
# Create your models here.

# Toggle the printing of debug information about the testcases executed to the
# console.
DEBUG_PRINT_TESTS = False


class Problem(AbstractProgrammingProblem):
    """
    A coding problem.

    A coding problem has all the properties of a problem, and
    a language and starter code
    """
    language = models.CharField(max_length=50,
                                choices=(('riscv', 'RiscV'),),
                                default='riscv')


class Submission(SubmissionPreprocessorMixin, AbstractSubmission):
    """
    A coding problem submission.
    """
    problem = models.ForeignKey(Problem, on_delete=models.CASCADE)

    def run_testcases(self, request, save=True):
        submitted_files = self.preprocessTags(default_file_name='code.asm')

        # TODO: Should we support multiple files?
        assert len(submitted_files) == 1
        submitted_code = submitted_files[0]['code']

        if DEBUG_PRINT_TESTS:
            print(f"\n[RiscV Test Case Report]\nSubmission: {submitted_files}")

        results, errors = [], []
        for testcase in self.problem.testcase_set.all():
            run, error = self.run_testcase(submitted_code, testcase, save=save)
            results.append(run)
            if error:
                errors.append(error)

        if DEBUG_PRINT_TESTS:
            print("\n")

        # Combine all errors
        final_error = '<br/>'.join(errors) if len(errors) > 0 else None
        return results, final_error

    def run_testcase(self, submitted_code, testcase, save=True):
        """
        Runs an individual test case.
        """

        run = testcase.run(submitted_code)
        error = None

        if 'passed_test' in run:
            passed = run['passed_test']
        else:
            passed = False
            error = run.get('exception', "The testcase could not be run")

        if save:
            TestRun.objects.create(submission=self, testcase=testcase,
                                   test_passed=passed)

        run['test_desc'] = testcase.description
        run['debug'] = False
        if testcase.is_visible:
            run['test_input'] = testcase.test_input.replace('\n', '<br />')
            run['debug'] = True
        else:
            run['test_input'] = None

        if DEBUG_PRINT_TESTS:
            print(f"Case {testcase}:\n\t- Result: {run}\n\t- Error: {error}")

        return run, error


class TestCase(AbstractTestCaseWithDescription):
    """
    A coding problem testcase.

    A testcase has an input and expected output and an optional description.
    The test input and expected output may or may not be visible to students.
    This is controlled by is_visible flag.
    """
    # TODO: This might need to be changed...
    # In particular the python model for what a test case should look like
    # probably doesn't match the riscv model.
    problem = models.ForeignKey(Problem, on_delete=models.CASCADE,
                                null=False, blank=False)
    pre_code = models.TextField(default="", blank=True)
    test_input = models.TextField()
    expected_output = models.TextField()

    def __str__(self):
        testcase = f"{self.test_input} -> {self.expected_output}"
        return f"{self.description} : {testcase}" if self.description else testcase

    def clean_fields(self, exclude=None):
        # Copied from Python
        super().clean_fields(exclude)
        if self.pk:
            if has_changed(self, 'problem_id'):
                raise ValidationError({
                    'problem': ['Reassigning a problem is not allowed.']
                })
            if self.problem.submission_set.all():
                clear = 'Submissions must be cleared before editing a testcase.'
                if has_changed(self, 'test_input'):
                    raise ValidationError({'test_input': [clear]})
                if has_changed(self, 'expected_output'):
                    raise ValidationError({'expected_output': [clear]})

    def run(self, code):
        runner = GenericLanguage(self.problem.language)
        return runner.run_test(code, self.test_input, self.expected_output, self.pre_code)


class TestRun(AbstractTestRun):
    """
    A coding problem testrun, created for each testcase on each submission.
    """
    submission = models.ForeignKey(Submission, on_delete=models.CASCADE)
    testcase = models.ForeignKey(TestCase, on_delete=models.CASCADE)

    def get_history(self):
        return {
            'visible': self.testcase.is_visible,
            'input': self.testcase.test_input,
            'output': self.testcase.expected_output,
            'passed': self.test_passed,
            'description': self.testcase.description
        }


post_delete.connect(testcase_delete, sender=TestCase)
post_delete.connect(problem_delete, sender=Problem)
