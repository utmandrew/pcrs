import json
from django.test import TestCase
from problems_riscv.riscv_language import RiscVSpecifics


class RiscVSpecificsTest(TestCase):
    def setUp(self) -> None:
        super().setUp()

        self.lang = RiscVSpecifics()

    def test_get_exec_trace_success(self):
        """
        Tests that correct RISCV asm code returns a JSON-encoded string with a
        traceEntries object.

        For tests that specify the content of the JSON, see
        `languages/riscv/tests`.
        """
        code = "addi t0, t0, 1"

        trace_str = self.lang.get_exec_trace(code)
        trace_obj = json.loads(trace_str)

        self.assertIn("traceEntries", trace_obj)

    def test_get_exec_trace_failure(self):
        """
        Tests that incorrect RISCV asm code returns a JSON-encoded string with a
        traceError object. The error is a string
        """
        code = "bad syntax"

        trace_str = self.lang.get_exec_trace(code)
        trace_obj = json.loads(trace_str)

        self.assertIn("traceError", trace_obj)
        self.assertIsInstance(trace_obj["traceError"], str)

    # TODO: Eventually we should test `run_test`.
