from django.contrib import admin
from django.conf import settings

from problems_riscv.models import Problem, Submission, TestCase, TestRun
from problems.admin import ProblemAdmin, SubmissionAdmin

# Register your models here.

if 'problems_riscv' in settings.INSTALLED_PROBLEM_APPS:
    admin.site.register(Problem, ProblemAdmin)
    admin.site.register(Submission, SubmissionAdmin)
    admin.site.register(TestCase)
    admin.site.register(TestRun)
