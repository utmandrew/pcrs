import json
from django import test
from .riscv_server import RiscVCodeModel, RiscVExecutorServer, new_riscv_executor_server

# Note: We don't test riscv_server_connect.py since that is considered an
# implementation detail.


class RiscvExecutorServerTests(test.TestCase):
    """
    Tests (riscv_server.py) RiscVExecutorServer and (indirectly)
    `new_riscv_executor_server`.
    """

    def setUp(self) -> None:
        self.server = new_riscv_executor_server()

    def tearDown(self) -> None:
        self.server.__exit__()

    def test_is_server_a_riscv_executor_server(self):
        self.assertIsInstance(self.server, RiscVExecutorServer)

    def test_can_create_code_model(self):
        model = self.server.new_code_model("")
        self.assertIsInstance(model, RiscVCodeModel)


class RiscVCodeModelTests(test.TestCase):
    """
    Tests RiscVCodeModel.
    """

    def setUp(self):
        self.server = new_riscv_executor_server()

    def tearDown(self):
        self.server.__exit__()

    def test_model_is_started_at_first(self):
        m = self.server.new_code_model("li a0, 10")
        self.assertEqual(m.status, RiscVCodeModel.STARTED)

    def test_can_create_model_with_invalid_code(self):
        m = self.server.new_code_model("dafsdfasdfadsfdasfads")
        self.assertEqual(m.status, RiscVCodeModel.STARTED)

    def test_invalid_code_run_status(self):
        m = self.server.new_code_model("dafsdfasdfadsfdasfads")
        m.run()
        self.assertEqual(m.status, RiscVCodeModel.FAILURE)

    def test_invalid_code_run_produces_error_message(self):
        m = self.server.new_code_model("dafsdfasdfadsfdasfads")
        m.run()
        error_message = m.get_error_message()
        self.assertNotEquals(error_message, "")
        self.assertIsInstance(error_message, str)

    def test_valid_code_run_status(self):
        m = self.server.new_code_model("li a0, 10")
        m.run()
        self.assertEqual(m.status, RiscVCodeModel.SUCCESS)

    def test_valid_code_run_produces_json_trace(self):
        m = self.server.new_code_model("li a0, 10")
        m.run()

        trace = m.get_trace_as_json()

        # Must be non-empty string
        self.assertNotEquals(trace, "")
        self.assertIsInstance(trace, str)

        # Must parse as valid JSON (Shouldn't throw)
        obj = json.loads(trace)

        # All valid traces should contain a `traceEntries` array.
        self.assertIn("traceEntries", obj)
        self.assertIsInstance(obj["traceEntries"], list)


class TraceTests(test.TestCase):
    """
    Tests RiscVCodeModel traces.
    """

    EMPTY_CODE = ""

    ADDI_SEQUENCE = """
    addi t0, t0, 1
    addi t0, t0, 2
    addi t0, t0, 3
    addi t0, t0, 4
    addi t0, t0, 5
    """

    SW_LW = """
    addi t0, x0, 12
    sw t0, 0(sp)
    lw t1, 0(sp)
    """

    ADD_t0_t1_t2 = """
    add t0, t1, t2
    """

    def setUp(self):
        self.server = new_riscv_executor_server()

    def tearDown(self):
        self.server.__exit__()

    def makeTraceMustSucceed(self, asm_code: str):
        """
        Executes `asm_code`, asserts this is a SUCCESS, and sets following
        instance variables:

        - `self.code_model` - the code model
        - `self.trace_str` - the JSON-encoded string trace.
        - `self.trace_obj` - the result of loading the aforementioned string as
          a python object.
        """
        self.code_model = self.server.new_code_model(asm_code)
        self.code_model.run()

        # As a convenience, print error message in case of failure.
        if self.code_model.status == RiscVCodeModel.FAILURE:
            print("ERROR MESSAGE:")
            print("\n")
            print(self.code_model.get_error_message())
            print("\n")

        self.assertEqual(self.code_model.status, RiscVCodeModel.SUCCESS)
        self.trace_str = self.code_model.get_trace_as_json()
        self.trace_obj = json.loads(self.trace_str)

    def getTraceEntries(self) -> list:
        """
        Returns the traceEntries array from the trace.

        Precondition: Must execute `self.makeTraceMustSucceed` before this.
        """
        return self.trace_obj["traceEntries"]

    def test_empty_code_has_single_step(self):
        self.makeTraceMustSucceed(self.EMPTY_CODE)
        self.assertEqual(len(self.getTraceEntries()), 1)

    def test_steps_form_sequence_starting_at_zero_in_correct_order(self):
        self.makeTraceMustSucceed(self.ADDI_SEQUENCE)
        self.assertEqual(len(self.getTraceEntries()), 6)

        for i, entry in enumerate(self.getTraceEntries()):
            step = entry["steps"]
            self.assertEqual(i, step)

    def test_has_program_counter(self):
        self.makeTraceMustSucceed(self.ADDI_SEQUENCE)

        for entry in self.getTraceEntries():
            pc = entry["programCounter"]
            self.assertIsInstance(pc, int)

    def test_exists_32_registers(self):
        self.makeTraceMustSucceed(self.ADDI_SEQUENCE)

        for entry in self.getTraceEntries():
            registers = entry["registers"]
            self.assertIsInstance(registers, list)
            self.assertEqual(len(registers), 32)
            for reg in registers:
                self.assertIsInstance(reg, int)

    def test_zero_register_is_zero(self):
        self.makeTraceMustSucceed(self.ADDI_SEQUENCE)

        for entry in self.getTraceEntries():
            registers = entry["registers"]
            x0 = registers[0]
            self.assertEqual(x0, 0)

    def test_exists_32_floating_point_registers(self):
        self.makeTraceMustSucceed(self.ADDI_SEQUENCE)

        for entry in self.getTraceEntries():
            registers = entry["floatingPointRegisters"]
            self.assertIsInstance(registers, list)
            self.assertEqual(len(registers), 32)

            # Note floating point registers are passed as integers in the JSON.
            for reg in registers:
                self.assertIsInstance(reg, int)

    def test_has_accesses(self):
        self.makeTraceMustSucceed(self.SW_LW)

        for entry in self.getTraceEntries():
            accesses = entry["accesses"]
            self.assertIsInstance(accesses, list)

    def test_has_no_accesses_in_step_0(self):
        self.makeTraceMustSucceed(self.SW_LW)
        entries = self.getTraceEntries()
        self.assertEqual(len(entries[0]["accesses"]), 0)

    def test_has_accesses_to_previous_pc_address(self):
        # Remark: The access tested here corresponds to fetching the
        # instruction.

        self.makeTraceMustSucceed(self.SW_LW)
        entries = self.getTraceEntries()

        for i in range(1, len(entries)):
            prev_pc = entries[i-1]["programCounter"]
            accesses = entries[i]["accesses"]

            self.assertTrue(
                any(
                    (
                        access["type"] == "ReadMemory"
                        and access["address"] == prev_pc
                        and access["size"] == 4
                    ) for access in accesses
                )
            )

    def test_accesses_store_to_sp_address(self):
        self.makeTraceMustSucceed(self.SW_LW)
        entries = self.getTraceEntries()

        sp = entries[0]["registers"][2]

        relevant_entry = entries[2]
        accesses = relevant_entry["accesses"]

        self.assertTrue(
            any(
                (
                    access["type"] == "WriteMemory"
                    and access["address"] == sp
                    and access["size"] == 4
                    and access["val"] == 12
                ) for access in accesses
            )
        )

    def test_accesses_load_from_sp_address(self):
        self.makeTraceMustSucceed(self.SW_LW)
        entries = self.getTraceEntries()

        sp = entries[0]["registers"][2]

        relevant_entry = entries[3]
        accesses = relevant_entry["accesses"]

        self.assertTrue(
            any(
                (
                    access["type"] == "ReadMemory"
                    and access["address"] == sp
                    and access["size"] == 4
                    and access["val"] == 12
                ) for access in accesses
            )
        )

    def test_accesses_to_registers(self):
        self.makeTraceMustSucceed(self.ADD_t0_t1_t2)

        entries = self.getTraceEntries()

        relevant_entry = entries[1]
        accesses = relevant_entry["accesses"]

        # ADD t0, t1, t2 should WRITE to t0, and READ FROM t1 and t2.

        self.assertEqual(
            len(list(
                a for a in accesses if a["type"] == "WriteReg")),
            1
        )
        self.assertEqual(
            len(list(
                a for a in accesses if a["type"] == "ReadReg")),
            2
        )

        # Should have a write to t0.
        write = list(a for a in accesses if a["type"] == "WriteReg")[0]

        self.assertEqual(write["name"], "t0")
        self.assertEqual(write["number"], 5)

        # Remark: Using `list` is necessary, else python would mutablyexhaust
        # the generator the first time it was used.
        reads = list(a for a in accesses if a["type"] == "ReadReg")

        # Should have a read from t1 and a read from t2.
        self.assertEqual(
            len(list(a for a in reads if a["name"]
                == "t1" and a["number"] == 6)),
            1
        )

        self.assertEqual(
            len(list(a for a in reads if a["name"]
                == "t2" and a["number"] == 7)),
            1
        )
