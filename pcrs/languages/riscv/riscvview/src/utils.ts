function asHex(n: number | undefined) {
  if (n === undefined) {
    return "???";
  } else {
    return "0x" + n.toString(16).toUpperCase();
  }
}

/** Returns the input values interprested as a 32-bit signed integer
 *
 * `value` must be an array of four numbers, where each number represents a
 * byte. The value is interpreted in little-endian format.
 */
function reconstructSignedWordValue(value: number[]) {
  console.assert(value.length === 4);

  return value[0] | (value[1] << 8) | (value[2] << 16) | (value[3] << 24);
}

/** Like `reconstructSignedWordValue`, but interprets `value` as a 32-bit
 * unsigned integer.
 */
function reconstructUnsignedWordValue(value: number[]) {
  return reconstructSignedWordValue(value) >>> 0;
}

export { asHex, reconstructSignedWordValue, reconstructUnsignedWordValue };
