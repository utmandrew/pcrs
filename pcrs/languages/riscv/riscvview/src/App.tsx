import React, { useRef, useState } from "react";

import { Registers, makeRegisterDataArray } from "./components/Registers";
import { Instruction } from "./components/Instruction";
import { Steps } from "./components/Steps";

import { Trace, traceDataIsError } from "./trace/trace";
import { TRACE_TMP_STR } from "./trace/debug";
import { useMemo } from "react";
import { Memory } from "./components/Memory";
import { Config } from "./components/Config";
import { DEFAULT_CONFIG } from "./configurations";
import { Button } from "./components/Button";
import { Source } from "./components/Source";
import { LineToSteps } from "./trace/source";
import { Help } from "./components/Help";

const SHOW_DEBUGGING_FEATURES = false;

function App({ trace: trace_ }: { trace?: string }) {
  const [traceData, setTrace] = useState(JSON.parse(trace_ || TRACE_TMP_STR));
  const [trace] = useMemo(() => {
    if (traceDataIsError(traceData)) {
      return [undefined];
    } else {
      return [new Trace(traceData)];
    }
  }, [traceData]);

  if (trace === undefined) {
    return (
      <div>
        Error during execution: <br />
        <code style={{ whiteSpace: "pre-line" }}>{traceData.traceError}</code>
        {SHOW_DEBUGGING_FEATURES ? (
          <DebuggingFeatures setTrace={setTrace} defaultTrace={""} />
        ) : (
          <></>
        )}
      </div>
    );
  } else {
    return (
      <div>
        <TraceView trace={trace} />
        {SHOW_DEBUGGING_FEATURES ? (
          <DebuggingFeatures setTrace={setTrace} defaultTrace={trace_ ?? ""} />
        ) : (
          <></>
        )}
      </div>
    );
  }
}

function TraceView({ trace }: { trace: Trace }) {
  const [tab, setTab] = React.useState<"trace" | "cfg" | "help">("trace");

  const [step, setStep] = React.useState(0);
  const [config, setConfig] = React.useState(DEFAULT_CONFIG);

  console.assert(trace.getTraceEntry(step).steps === step);
  const traceEntry = trace.getTraceEntry(step);

  return (
    <div className="App">
      <div className="container p-5">
        <div className="tabs p-2">
          <a
            className={
              "tab tab-lg tab-lifted" + (tab === "trace" ? " tab-active" : "")
            }
            onClick={() => setTab("trace")}
          >
            Trace
          </a>
          <a
            className={
              "tab tab-lg tab-lifted" + (tab === "cfg" ? " tab-active" : "")
            }
            onClick={() => setTab("cfg")}
          >
            Configs
          </a>
          <a
            className={
              "tab tab-lg tab-lifted" + (tab === "help" ? " tab-active" : "")
            }
            onClick={() => setTab("help")}
          >
            Help
          </a>
        </div>

        <div className={tab === "help" ? "" : "hidden"}>
          <div className="ml-3 text">
            <Help />
          </div>
        </div>

        <div className={tab === "cfg" ? "" : "hidden"}>
          <Config config={config} setConfig={setConfig} />
        </div>

        <div className={tab === "trace" ? "" : "hidden"}>
          <Steps
            config={config}
            step={step}
            lastStep={trace.getLastStep()}
            setStep={setStep}
          />
          <div className="container flex w-11/12">
            <div className="w-10/12 p-3">
              <Instruction config={config} instruction={traceEntry.statement} />
              <Source
                config={config}
                source={trace.data.sourceFile!}
                currStep={step}
                currLineNumber={traceEntry.statement?.sourceLineNumber}
                lineToSteps={new LineToSteps(trace.data)}
                gotoStepFn={setStep}
              />
            </div>
            <div className="w-2/12 p-3">
              <Registers
                config={config}
                registers={makeRegisterDataArray(
                  traceEntry.registers,
                  traceEntry.accesses
                )}
              />
            </div>
          </div>
          <div className="container flex w-11/12">
            <div className="w-full p-3">
              <Memory
                config={config}
                memoryModel={trace.memoryModel}
                step={step}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

function DebuggingFeatures({
  defaultTrace,
  setTrace,
}: {
  defaultTrace: string;
  setTrace: React.Dispatch<any>;
}) {
  const traceTextArea = useRef<HTMLTextAreaElement>(null);

  return (
    <div className="p-4">
      <h1 className="font-bold">Debugging Features</h1>
      <br /> Trace:
      <br />
      <textarea
        className="w-1/2 bg-neutral-200"
        ref={traceTextArea}
        defaultValue={defaultTrace}
      />
      <br />
      <Button
        isActive={true}
        onClick={() => {
          console.log(traceTextArea.current?.value);
          setTrace(JSON.parse(traceTextArea.current?.value!));
        }}
      >
        setTrace
      </Button>
    </div>
  );
}

export default App;
