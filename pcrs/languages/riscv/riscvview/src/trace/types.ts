interface TraceData {
  readonly traceEntries?: readonly TraceEntry[];
  readonly sourceFile?: string;
  readonly traceError?: string;
}

interface TraceEntry {
  readonly steps: number;
  readonly registers: readonly number[];
  readonly programCounter: number;
  readonly instructionPC: number;
  readonly floatingPointRegisters: readonly number[];
  readonly accesses: readonly TraceAccess[];
  readonly statement?: TraceStatement;
}

interface TraceStatement {
  readonly actualAsm: string;
  readonly address: number;
  readonly sourceLine: string;
  readonly sourceLineNumber: number;
}

interface AccessReadMemory {
  readonly type: "ReadMemory";
  readonly address: number;
  readonly size: number;
  readonly val: number;
}

interface AccessWriteMemory {
  readonly type: "WriteMemory";
  readonly address: number;
  readonly size: number;
  readonly val: number;
}

interface AccessReadReg {
  readonly type: "ReadReg";
  readonly name: string;
  readonly number: number;
}

interface AccessWriteReg {
  readonly type: "WriteReg";
  readonly name: string;
  readonly number: number;
}

type TraceAccess =
  | AccessReadMemory
  | AccessWriteMemory
  | AccessReadReg
  | AccessWriteReg;

interface MemoryModel {
  /** TODO: Are we expected to support wide acceses? */
  readonly locations: ReadonlyMap<number, MemoryLocation>;
}

interface MemoryLocation {
  /** Should be a multiple of four. */
  readonly baseAddress: number;
  readonly writes: readonly MemoryLocationAccess[];
  readonly reads: readonly MemoryLocationAccess[];
  readonly values: readonly MemoryLocationValue[];
}

interface MemoryLocationAccess {
  readonly kind: "read" | "write";
  /** The step when this memory access occurred. */
  readonly step: number;
  /** Width of the memory access: 1, 2 or 4 */
  readonly width: number;
  /** Between 0 and 3 */
  readonly offset: number;
  /** The value corresponding to this read or write. */
  readonly value: number;
}

interface MemoryLocationValue {
  readonly step: number;
  /** Array of 4 numbers, corresponding to four bytes. */
  readonly value: (number | undefined)[];
}

export type {
  TraceData,
  TraceEntry,
  TraceAccess,
  TraceStatement,
  AccessReadMemory,
  AccessWriteMemory,
  AccessReadReg,
  AccessWriteReg,
  MemoryModel,
  MemoryLocation,
  MemoryLocationAccess,
  MemoryLocationValue,
};
