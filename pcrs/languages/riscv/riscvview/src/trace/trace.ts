import { buildMemoryModel } from "./memory";
import type { MemoryModel, TraceData, TraceEntry } from "./types";

function traceDataIsError(data: TraceData) {
  return data.traceError !== undefined;
}

class Trace {
  readonly data: TraceData;
  readonly memoryModel: MemoryModel;

  constructor(data: TraceData) {
    console.log("Creating trace");

    this.data = data;
    this.memoryModel = buildMemoryModel(this.data.traceEntries!);

    console.log(this.data);
    console.log(this.memoryModel);
  }

  getLastStep(): number {
    return this.data.traceEntries!.length - 1;
  }

  getTraceEntry(step: number): TraceEntry {
    return this.data.traceEntries![step];
  }
}

// "accesses":[{"type":"ReadMemory","address":4194316,"size":4,"val":10814739},{"type":"ReadReg","name":"a0"},{"type":"WriteReg","name":"a0"}]

export { Trace, traceDataIsError };
