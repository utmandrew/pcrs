import { TraceData } from "./types";

class LineToSteps {
  readonly lineToSteps: ReadonlyMap<number, readonly number[]>;

  constructor(traceData: TraceData) {
    const lineToSteps = new Map<number, number[]>();

    traceData.traceEntries!.forEach((traceEntry) => {
      if (traceEntry.statement !== undefined) {
        if (!lineToSteps.has(traceEntry.statement.sourceLineNumber)) {
          lineToSteps.set(traceEntry.statement.sourceLineNumber, []);
        }

        lineToSteps
          .get(traceEntry.statement.sourceLineNumber)!
          .push(traceEntry.steps);
      }
    });

    this.lineToSteps = lineToSteps;
  }

  public getNextStepAtLine(
    lineNumber: number,
    currentStep: number
  ): number | undefined {
    const steps = this.lineToSteps.get(lineNumber) ?? [];

    // Search for smallest step > currentStep
    // TODO: Is it worth optimizing this to use binary search?
    for (let i = 0; i < steps.length; i++) {
      if (steps[i] > currentStep) return steps[i];
    }

    return undefined;
  }

  public getPrevStepAtLine(
    lineNumber: number,
    currentStep: number
  ): number | undefined {
    const steps = this.lineToSteps.get(lineNumber) ?? [];

    // Search for greatest step < currentStep
    // TODO: Is it worth optimizing this to use binary search?
    for (let i = steps.length - 1; i >= 0; i--) {
      if (steps[i] < currentStep) return steps[i];
    }

    return undefined;
  }
}

export { LineToSteps };
