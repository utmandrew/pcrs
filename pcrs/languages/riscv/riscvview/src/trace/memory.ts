import {
  TraceAccess,
  TraceEntry,
  MemoryModel,
  MemoryLocation,
  MemoryLocationAccess,
  MemoryLocationValue,
} from "./types";

function buildMemoryModel(traceEntries: readonly TraceEntry[]): MemoryModel {
  // Group together all operations that affect the same word.
  const accessMap = buildAccessMap(traceEntries);

  // For each word, process all reads and writes, to create a model
  // of the state of that word at each relevant step.
  let locations: Map<number, MemoryLocation> = new Map();
  for (const [baseAddr, accesses] of accessMap.entries()) {
    locations.set(baseAddr, buildMemoryLocation(accesses, baseAddr));
  }

  return { locations };
}

function buildMemoryLocation(
  accesses: (TraceAccess & { step: number })[],
  baseAddress: number
): MemoryLocation {
  let reads: MemoryLocationAccess[] = [];
  let writes: MemoryLocationAccess[] = [];
  let readsOrWrites: MemoryLocationAccess[] = [];

  // Organize accesses into reads / writes / both
  accesses.forEach((access) => {
    switch (access.type) {
      case "ReadMemory":
        const r: MemoryLocationAccess = {
          kind: "read",
          offset: access.address - baseAddress,
          step: access.step,
          value: access.val,
          width: access.size,
        };
        reads.push(r);
        readsOrWrites.push(r);
        break;

      case "WriteMemory":
        const w: MemoryLocationAccess = {
          kind: "write",
          offset: access.address - baseAddress,
          step: access.step,
          value: access.val,
          width: access.size,
        };
        writes.push(w);
        readsOrWrites.push(w);
        break;
    }
  });

  // Sort operations: primarily by step (when it happened). Disambiguate
  // (hypothetical) simultaneous operations by ordering reads first (i.e., we
  // interpret a "simultaneous" read/write as a read followed by a write).
  //
  // Remark: I'm not actually sure if RISCV will ever produce such simultaneous
  // operations.
  reads.sort(orderByStepWithReadsFirst);
  writes.sort(orderByStepWithReadsFirst);
  readsOrWrites.sort(orderByStepWithReadsFirst);

  // Assertion: we only process addresses that are involved in at least one read
  // or one write.
  console.assert(readsOrWrites.length > 0);

  let values: MemoryLocationValue[] = [
    { step: 0, value: [undefined, undefined, undefined, undefined] },
  ];

  // Reads and writes are sorted primarily by step and secondarily by ordering
  // reads before writes.
  //
  // Hence if we observe a write to a byte of unknown (undefined) value, that
  // means the initial state cannot be extracted from the memory access data; we
  // may regard it as effectively uninitialized memory.
  //
  // Otherwise, if a read precedes all write to that byte, we may assume the
  // memory address always contained that value.
  //
  // This is reasonable in general. It might cause us issues if we ever decide
  // to support Memory Mapped IO or similar such features, however.

  // Process memory events in sorted order (as above)
  for (const op of readsOrWrites) {
    // knownBytes represents our current best-guess as to the state of
    // the word at position `baseAddress` at time `op.step`.
    let knownBytes = values[values.length - 1].value;

    if (op.kind === "read") {
      for (const b of [0, 1, 2, 3]) {
        if (operationAffectsByte(op, b)) {
          if (knownBytes[b] === undefined) {
            // We have gained knowledge of the state of knownBytes[b]. Since it
            // was undefined, this implies we haven't written to it yet. Store
            // this as the value at _all_ steps so far considered.
            for (const e of values) {
              e.value[b] = operationGetByteValue(op, b);
            }
          } else {
            // We already knew the state of bytes[b] at time step=currStep.
            // Simply assert that we were correct.
            console.assert(
              values[values.length - 1].value[b] ===
                operationGetByteValue(op, b)
            );
          }
        }
      }
    } else if (op.kind === "write") {
      // Push a copy of the current known state of this memory address.
      // We will update it according to this write.
      values.push({ step: op.step, value: [...knownBytes] });
      for (const b of [0, 1, 2, 3]) {
        // Update all bytes affected by this write.
        if (operationAffectsByte(op, b)) {
          values[values.length - 1].value[b] = operationGetByteValue(op, b);
        }
      }
    }
  }

  return {
    baseAddress,
    reads,
    writes,
    values,
  };
}

function operationAffectsByte(op: MemoryLocationAccess, byte: number): boolean {
  return op.offset <= byte && byte < op.offset + op.width;
}

function operationGetByteValue(op: MemoryLocationAccess, byte: number): number {
  console.assert(operationAffectsByte(op, byte));

  const offsetIntoValue = byte - op.offset;
  return (op.value & (0xff << (offsetIntoValue * 8))) >>> (offsetIntoValue * 8);
}

function orderByStepWithReadsFirst(
  a: MemoryLocationAccess,
  b: MemoryLocationAccess
): number {
  if (a.step !== b.step) {
    return a.step - b.step;
  } else {
    if (a.kind !== b.kind) {
      return a.kind === "read" ? -1 : 1;
    } else {
      return 0;
    }
  }
}

function baseAddress(address: number): number {
  return address - (address % 4);
}

function buildAccessMap(
  traceEntries: readonly TraceEntry[]
): Map<number, Array<TraceAccess & { step: number }>> {
  let accessMap = new Map();

  for (const traceEntry of traceEntries) {
    const step = traceEntry.steps;
    for (const access of traceEntry.accesses) {
      if (access.type === "ReadMemory" || access.type === "WriteMemory") {
        const base = baseAddress(access.address);
        if (accessMap.has(base)) {
          accessMap.get(base).push({ ...access, step: step });
        } else {
          accessMap.set(base, [{ ...access, step: step }]);
        }
      }
    }
  }

  return accessMap;
}

export { buildMemoryModel };
