interface Configurations {
  base: "dec" | "hex";
  hcol: "def" | "alt";
}

function getReadBgColorClass(cfg: Configurations) {
  return cfg.hcol === "def" ? "bg-green-300" : "bg-blue-800 text-white";
}

function getWriteBgColorClass(cfg: Configurations) {
  return cfg.hcol === "def" ? "bg-red-300" : "bg-amber-400";
}

const DEFAULT_CONFIG: Configurations = { base: "dec", hcol: "def" };

export type { Configurations };
export { DEFAULT_CONFIG, getReadBgColorClass, getWriteBgColorClass };
