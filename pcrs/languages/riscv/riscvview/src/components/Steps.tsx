import { useState } from "react";
import { Configurations } from "../configurations";
import { Button } from "./Button";

interface StepProps {
  readonly config: Configurations;
  readonly step: number;
  readonly lastStep: number;
  readonly setStep: (s: number) => void;
}

function Steps(props: StepProps) {
  const { step, lastStep, setStep } = props;

  return (
    <div className="container w-5/6 text-left">
      <div className="inline-flex items-center">
        <Button isActive={step > 0} onClick={() => setStep(step - 1)}>
          Previous
        </Button>

        <span className="inline-block">
          <b>Step:</b> {step} <b> of </b> {lastStep}
        </span>

        <Button isActive={step < lastStep} onClick={() => setStep(step + 1)}>
          Next
        </Button>

        <GotoStep {...props} />
      </div>
    </div>
  );
}

function GotoStep({ step, lastStep, setStep }: StepProps) {
  const [stepText, setStepText] = useState("");

  return (
    <div className="ml-6">
      <Button
        isActive={0 <= parseInt(stepText) && parseInt(stepText) <= lastStep}
        onClick={() => setStep(parseInt(stepText))}
      >
        Go To Step
      </Button>
      <input
        type="text"
        className="input input-bordered input-sm w-40 bg-gray-100"
        placeholder="Type a number here"
        title="Type a step number here to go to that step. Note this means go to the n-th step (not necessarily line n)."
        value={stepText}
        onChange={(e) => setStepText(e.target.value)}
      />
    </div>
  );
}

export { Steps };
