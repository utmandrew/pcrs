import { useState } from "react";
import {
  Configurations,
  getReadBgColorClass,
  getWriteBgColorClass,
} from "../configurations";
import { REG_ABI_NAMES, REG_NAMES } from "../trace/constants";
import { TraceAccess } from "../trace/types";
import { asHex } from "../utils";
import { Switch } from "./Switch";

interface RegistersProps {
  readonly registers: readonly RegisterData[];
  readonly config: Configurations;
}

function Registers({ config, registers }: RegistersProps): JSX.Element {
  const [showUntouchedRegisters, setShowUntouchedRegisters] = useState(true);

  return (
    <div className="container">
      <table className="w-full text-left">
        <thead className="container h-12 flex items-center text-left py-4 bg-gray-300 border-l-8 border-r-8 border-gray-300">
          <tr className="flex w-full">
            <th className="w-28 p-2">Register</th>
            <th className="w-16 p-2">Value</th>
            <th className="w-40 p-2 justify-end">
              <Switch
                checked={showUntouchedRegisters}
                setChecked={setShowUntouchedRegisters}
                id="show-untouched-addresses"
              >
                Show All
              </Switch>
            </th>
          </tr>
        </thead>
        <tbody className="flex flex-col w-full overflow-y-scroll h-48 border-gray-300 border-b-8 border-l-8 border-r-8">
          {registers.map((r) => {
            return (
              <RegisterRow
                key={r.name}
                config={config}
                showUntouchedRegisters={showUntouchedRegisters}
                {...r}
              />
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

interface RegisterData {
  readonly name: string;
  readonly val: number;
  readonly hasRead: boolean;
  readonly hasWrite: boolean;
}

type RegisterRowProps = RegisterData & {
  readonly config: Configurations;
  readonly showUntouchedRegisters: boolean;
};

function RegisterRow({
  name,
  val,
  config,
  hasRead,
  hasWrite,
  showUntouchedRegisters,
}: RegisterRowProps): JSX.Element {
  const backgroundCss = hasWrite
    ? getWriteBgColorClass(config)
    : hasRead
    ? getReadBgColorClass(config)
    : "";

  if (showUntouchedRegisters || hasRead || hasWrite) {
    return (
      <tr className={"flex w-full " + backgroundCss}>
        <td className="w-28 p-2">{name}</td>
        <td className="w-40 p-2 font-mono">
          {config.base === "hex" ? asHex(val) : val}
        </td>
        <td
          className="w-10 p-2"
          title={hasWrite ? "Write" : hasRead ? "Read" : ""}
        >
          {hasWrite ? "W" : hasRead ? "R" : ""}
        </td>
      </tr>
    );
  } else {
    return <></>;
  }
}

/** Returns a RegisterTable, given an array of 32 register values `regTrace`.
 *
 * The array `regTrace` is assumed to be in register number order, starting with
 * `x0` (which should always contain the value 0), up til `x31`.
 */
function makeRegisterDataArray(
  regTrace: readonly number[],
  accessInfo: readonly TraceAccess[]
): RegisterData[] {
  let out: RegisterData[] = [];
  for (let i = 0; i < 32; i++) {
    let hasRead = false;
    let hasWrite = false;
    for (const a of accessInfo) {
      if (a.type === "ReadReg" && a.number === i) {
        hasRead = true;
      }
      if (a.type === "WriteReg" && a.number === i) {
        hasWrite = true;
      }
    }

    out.push({
      name: `${REG_NAMES[i]} (${REG_ABI_NAMES[i]})`,
      val: regTrace[i],
      hasRead,
      hasWrite,
    });
  }

  return out;
}

export { Registers, makeRegisterDataArray };
