import { Configurations } from "../configurations";
import { TraceStatement } from "../trace/types";
import { asHex } from "../utils";

interface InstructionProps {
  config: Configurations;
  instruction: TraceStatement | undefined;
}

function Instruction({ config, instruction }: InstructionProps) {
  if (instruction === undefined) {
    return <div className="container h-12 text-left p-4 bg-gray-300"></div>;
  } else {
    const { address, actualAsm, sourceLine } = instruction;

    return (
      <div className="container h-12 flex items-center p-4 bg-gray-300">
        <span className="font-mono" title="Instruction Address">
          {config.base === "hex" ? asHex(address) : address}
        </span>
        <span className="px-6" />
        <span className="font-mono">{actualAsm}</span>{" "}
        {showHint(instruction) ? (
          <div
            title="This shows the actual CPU instruction. Some lines of code, like 'li', consist of pseudoinstructions, so this might differ from what you wrote."
            className="px-12 text-center"
          >
            ❓
          </div>
        ) : (
          <></>
        )}
      </div>
    );
  }
}

function showHint(instruction: TraceStatement | undefined) {
  if (instruction === undefined) {
    return false;
  } else {
    const source_words = instruction.sourceLine.trim().split(" ");
    const actual_words = instruction.actualAsm.trim().split(" ");
    if (source_words[0] !== actual_words[0]) {
      return true;
    }
    return false;
  }
}

export { Instruction };
