function Help() {
  return (
    <div className="prose">
      <h1>Riscvview Usage Guide</h1>

      <h2>Configurations</h2>

      <p>
        Riscvview supports a few configuration options. To change these, head
        over to the Configs tab.
      </p>

      <p>
        Use the "Base" option to select whether to display values in decimal or
        hexadecimal. Note that some values are always displayed in decimal,
        irrespective of base choice.
      </p>

      <p>
        You can also switch over to an alternate colour scheme. By default,
        Riscvview shows reads in green and writes in red. If you prefer, you can
        switch to showing reads in blue and writes in orange.
      </p>

      <h2>Stepping Trough Code</h2>

      <p>There are three ways to step through your code.</p>

      <p>
        The first way to step through your code is to use the "Previous" and
        "Next" buttons. This will step one instruction at a time.
      </p>

      <p>
        Alternatively, you can fill out a step number besides the "Go To Step"
        button. The button will be greyed out unless you input a valid step
        number. If you then press this button, the trace display will
        immediately switch to the desired step. Note, however, that the step
        number might not match the line number in your source code.
      </p>

      <p>
        The last way to step through your code is to use the &lt;&lt; and
        &gt;&gt; icons besides each line of code. These buttons will
        respectively rewind or fast forward your code up to the next time that
        line is reached. Note these buttons only show up if it is indeed
        possible to reach the line stepping back or forwards, respectively.
        These are particularly useful in loops, as you can repeatedly press them
        to go to the previous or next instance of a certain line in a loop.
      </p>

      <h3>Remark: Pseudoinstructions</h3>

      <p>
        Some lines of code may be assembled to multiple separate machine
        instructions. For instance, the li pseudoinstruction may compile to
        multiple instructions if the immediate value is sufficiently large. In
        this case, stepping through your code might not change the highlighted
        line. You can nonetheless keep track of the current assembly instruction
        by observing that the actual assembly instruction is displayed above the
        source code listing. For your convenience, the address where the
        instruction is located (i.e. the Program Counter) is also displayed
        (either in decimal or in hexadecimal, depending on your configs).
      </p>

      <p>
        Riscvview also does a best-effort attempt at detecting
        pseudoinstructions. If these are detected, we display a ❓ icon, which
        you can hover over for more info.
      </p>

      <h2>Registers</h2>

      <p>
        Besides the source code listing, we also provide a view into register
        state. Use the "Show All" toggle to select whether to show all
        registers, or only those that were used by the current instruction.
      </p>

      <p>
        Used registers are highlighted, according to your configured colour
        scheme, indicating reads and writes.
      </p>

      <h2>Memory</h2>

      <p>
        Riscvview also provides a memory view. This works similarly to the
        register view. Note that individual bytes are grouped into 4-byte words.
        However, reads and writes are highlighted on a per-byte basis (e.g. a lb
        instruction will only highlight a single read byte, whereas a lw
        instruction will highlight the entire word).
      </p>

      <p>
        Byte values are always show in hexadecimal. Word values are displayed in
        decimal or hexadecimal according to your configuration.
      </p>
    </div>
  );
}

export { Help };
