import { Configurations } from "../configurations";

interface ConfigProps {
  config: Configurations;
  setConfig: React.Dispatch<any>;
}

function Config(props: ConfigProps) {
  return (
    <div>
      <ConfigBase {...props} />
    </div>
  );
}

function ConfigBase({ config, setConfig }: ConfigProps) {
  return (
    <>
      <div className="flex items-center">
        <span className="ml-3 px-2">Base:</span>
        <span className="inline-flex">
          <label htmlFor={"base-dec"} className="label justify-start">
            <RadioInput
              name="base"
              value="dec"
              get={() => config.base}
              set={(v) => setConfig({ ...config, base: v })}
              id="base-dec"
            />
            <span className="label-text px-2">Decimal</span>
          </label>
        </span>
        <span className="inline-flex">
          <label htmlFor={"base-hex"} className="label justify-start">
            <RadioInput
              name="base"
              value="hex"
              get={() => config.base}
              set={(v) => setConfig({ ...config, base: v })}
              id="base-hex"
            />
            <span className="label-text px-2">Hex</span>
          </label>
        </span>
      </div>
      <div className="flex items-center">
        <span className="ml-3 px-2">Read/Write Colour Scheme:</span>
        <span className="inline-flex">
          <label htmlFor={"hcol-def"} className="label justify-start">
            <RadioInput
              name="hcol"
              value="def"
              get={() => config.hcol}
              set={(v) => setConfig({ ...config, hcol: v })}
              id="hcol-def"
            />
            <span className="label-text px-2">Default (Green and Red)</span>
          </label>
        </span>
        <span className="inline-flex">
          <label htmlFor={"hcol-alt"} className="label justify-start">
            <RadioInput
              name="hcol"
              value="alt"
              get={() => config.hcol}
              set={(v) => setConfig({ ...config, hcol: v })}
              id="hcol-alt"
            />
            <span className="label-text px-2">
              Alternative (Blue and Orange)
            </span>
          </label>
        </span>
      </div>
    </>
  );
}

interface RadioInputProps {
  name: string;
  value: string;
  get: () => string;
  set: (value: string) => void;
  id?: string;
}

function RadioInput({ name, value, get, set, id }: RadioInputProps) {
  return (
    <input
      type="radio"
      className="radio text-blue-400 border-blue-600 checked:bg-blue-900"
      value={value}
      name={name}
      checked={get() === value}
      onChange={() => set(value)}
      id={id}
    />
  );
}

export { Config };
