interface SwitchProps {
  checked: boolean;
  setChecked: (b: boolean) => void;
  id: string;
  children: React.ReactNode;
}

function Switch({ checked, setChecked, children, id }: SwitchProps) {
  // See https://tailwind-elements.com/docs/standard/forms/switch/
  return (
    <div className="form-check form-switch flex justify-end">
      <input
        className="form-check-input mx-2 appearance-none w-9 -ml-10 rounded-full float-left h-5 align-top bg-no-repeat bg-contain bg-gray-300 focus:outline-none cursor-pointer shadow-sm"
        type="checkbox"
        role="switch"
        checked={checked}
        onChange={() => setChecked(!checked)}
        id={id}
      ></input>
      <label htmlFor={id} className="form-check-label font-semibold">
        {children}
      </label>
    </div>
  );
}

export { Switch };
