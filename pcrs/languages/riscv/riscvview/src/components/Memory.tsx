import { useState } from "react";
import {
  Configurations,
  getReadBgColorClass,
  getWriteBgColorClass,
} from "../configurations";
import { MemoryLocation, MemoryModel } from "../trace/types";
import {
  asHex,
  reconstructSignedWordValue,
  reconstructUnsignedWordValue,
} from "../utils";

import "tw-elements";
import { Switch } from "./Switch";

interface MemoryProps {
  config: Configurations;
  memoryModel: MemoryModel;
  step: number;
}

function Memory({ config, memoryModel, step }: MemoryProps) {
  const [showUntouchedAddresses, setShowUntouchedAddresses] = useState(true);

  let addresses: MemoryLocation[] = [];
  for (const addr of memoryModel.locations.keys()) {
    addresses.push(memoryModel.locations.get(addr)!);
  }
  addresses.sort((a, b) => a.baseAddress - b.baseAddress);

  return (
    <div className="container border-l-8 border-r-8 border-b-8 border-gray-300">
      <table className="w-full text-left">
        <thead className="flex w-full bg-gray-300 font-bold py-2 ">
          <tr className="flex w-full">
            <th className="w-48 p-2">Mem Address</th>
            <th className="w-64 p-2">Bytes</th>
            <th className="w-40 p-2">Value</th>
            <th className="p-2">
              <Switch
                checked={showUntouchedAddresses}
                setChecked={setShowUntouchedAddresses}
                id="show-untouched-addresses"
              >
                Show All
              </Switch>
            </th>
          </tr>
        </thead>
        <tbody className="flex flex-col w-full overflow-y-scroll h-40">
          {addresses.map((addr) => {
            return (
              <MemoryRow
                key={addr.baseAddress}
                config={config}
                addr={addr}
                step={step}
                showUntouchedAddresses={showUntouchedAddresses}
              />
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

interface MemoryRowProps {
  config: Configurations;
  addr: MemoryLocation;
  step: number;
  showUntouchedAddresses: boolean;
}

function MemoryRow({
  config,
  addr,
  step,
  showUntouchedAddresses,
}: MemoryRowProps) {
  // TODO: Add option to show only memory addresses that have reads or writes.
  let touched = false;

  // TODO: Would a a binary search for value be faster?
  let d: (number | undefined)[] = [undefined, undefined, undefined, undefined];
  for (const v of addr.values) {
    if (v.step <= step) {
      d = v.value;
    }
  }

  let has_write_to_byte = [false, false, false, false];

  for (const w of addr.writes) {
    if (w.step === step) {
      for (const b of [0, 1, 2, 3]) {
        if (w.offset <= b && b < w.offset + w.width) {
          has_write_to_byte[b] = true;
          touched = true;
        }
      }
    }
  }

  let has_read_from_byte = [false, false, false, false];

  for (const r of addr.reads) {
    if (r.step === step) {
      for (const b of [0, 1, 2, 3]) {
        if (r.offset <= b && b < r.offset + r.width) {
          has_read_from_byte[b] = true;
          touched = true;
        }
      }
    }
  }

  const baseAddressElement = (
    <td className="w-48 p-2 font-mono">
      {config.base === "hex" ? asHex(addr.baseAddress) : addr.baseAddress}
    </td>
  );

  const byteClass = (b: number) =>
    has_write_to_byte[b]
      ? "w-16 p-2 font-mono " + getWriteBgColorClass(config)
      : has_read_from_byte[b]
      ? "w-16 p-2 font-mono " + getReadBgColorClass(config)
      : "w-16 p-2 font-mono bg-neutral-200";

  const byteTitle = (b: number) =>
    has_write_to_byte[b] ? "Write" : has_read_from_byte[b] ? "Read" : "";

  const bytesElement = (
    <>
      <td className={byteClass(0)} title={byteTitle(0)}>
        {asHex(d[0])}
      </td>
      <td className={byteClass(1)} title={byteTitle(1)}>
        {asHex(d[1])}
      </td>
      <td className={byteClass(2)} title={byteTitle(2)}>
        {asHex(d[2])}
      </td>
      <td className={byteClass(3)} title={byteTitle(3)}>
        {asHex(d[3])}
      </td>
    </>
  );

  const wordValueElement = (
    <td className="w-40 p-2 font-mono">
      {config.base === "hex"
        ? asHex(wordValue(d, false))
        : wordValue(d, false) ?? "???"}
    </td>
  );

  if (showUntouchedAddresses || touched) {
    return (
      <tr className="flex w-full text-left">
        {baseAddressElement}
        {bytesElement}
        {wordValueElement}
      </tr>
    );
  } else {
    return <></>;
  }
}

function wordValue(bytes: (number | undefined)[], signed: boolean) {
  // If _any_ of the bytes is unknown (represented by undefined), we cannot find
  // out the value of the word.
  if (bytes.includes(undefined)) {
    return undefined;
  }

  if (signed) {
    return reconstructSignedWordValue(bytes as number[]);
  } else {
    return reconstructUnsignedWordValue(bytes as number[]);
  }
}

export { Memory };
