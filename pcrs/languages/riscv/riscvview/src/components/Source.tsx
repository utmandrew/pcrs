import { Configurations } from "../configurations";
import { LineToSteps } from "../trace/source";

interface SourceProps {
  config: Configurations;
  source: string;
  currLineNumber: number | undefined;
  currStep: number;
  lineToSteps: LineToSteps;
  gotoStepFn: (step: number) => void;
}

function Source({
  config,
  source,
  currLineNumber,
  currStep,
  lineToSteps,
  gotoStepFn,
}: SourceProps) {
  const lines = source.split("\n");

  return (
    <div className="container overflow-x-scroll overflow-y-scroll h-48 border-gray-300 border-l-8 border-r-8 border-b-8">
      <table className="w-full text-left">
        <tbody className="flex flex-col w-full font-normal">
          {lines.map((l, i) => {
            return (
              <SourceLine
                key={i}
                config={config}
                line={l}
                lineNum={i + 1}
                currLineNumber={currLineNumber}
                prevStepAtLine={lineToSteps.getPrevStepAtLine(i + 1, currStep)}
                nextStepAtLine={lineToSteps.getNextStepAtLine(i + 1, currStep)}
                gotoStepFn={gotoStepFn}
              />
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

interface SourceLineProps {
  config: Configurations;
  lineNum: number;
  currLineNumber: number | undefined;
  line: string;
  prevStepAtLine: number | undefined;
  nextStepAtLine: number | undefined;
  gotoStepFn: (step: number) => void;
}

function SourceLine({
  lineNum,
  line,
  currLineNumber,
  prevStepAtLine,
  nextStepAtLine,
  gotoStepFn,
}: SourceLineProps) {
  return (
    <tr
      className={
        currLineNumber === lineNum ? "flex w-full  bg-green-300" : "flex w-full"
      }
    >
      <th
        className={
          "w-6 pr-0 select-none text-left font-mono font-normal" +
          (prevStepAtLine === undefined ? "" : " cursor-pointer")
        }
        onClick={() => {
          if (prevStepAtLine !== undefined) gotoStepFn(prevStepAtLine);
        }}
        title={prevStepAtLine === undefined ? "" : "Go back to this line."}
      >
        {prevStepAtLine === undefined ? "" : "<<"}
      </th>
      <th
        className={
          "w-6 pr-0 select-none text-left font-mono font-normal" +
          (nextStepAtLine === undefined ? "" : " cursor-pointer")
        }
        onClick={() => {
          if (nextStepAtLine !== undefined) gotoStepFn(nextStepAtLine);
        }}
        title={nextStepAtLine === undefined ? "" : "Fast-forward to this line."}
      >
        {nextStepAtLine === undefined ? "" : ">>"}
      </th>
      <th className="w-10 pr-3 select-none text-right font-mono font-normal">
        {lineNum}
      </th>
      <th
        className={
          "w-64 font-mono " +
          (currLineNumber === lineNum ? "font-extrabold" : "font-normal")
        }
        title={currLineNumber === lineNum ? "Current Line" : ""}
      >
        <pre>{line}</pre>
      </th>
    </tr>
  );
}

export { Source };
