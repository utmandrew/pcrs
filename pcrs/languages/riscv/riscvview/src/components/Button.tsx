import "./button.css";

interface ButtonProps {
  isActive: boolean;
  onClick: () => void;
  children: React.ReactNode;
}

/** A generic button which may be active or inactive. `onClick` is only invoked
 * if the button is active. */
function Button({ isActive, onClick, children }: ButtonProps) {
  return (
    <button
      className={`button ${isActive ? "button-active" : "button-inactive"}`}
      onClick={isActive ? onClick : () => {}}
    >
      {children}
    </button>
  );
}

export { Button };
