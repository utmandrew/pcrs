import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";

function mountReact(target_id: string, trace?: string) {
  console.log("Mounting react - riscvview");

  if (trace !== undefined) {
    console.log(trace);
  }

  const rootElem = document.getElementById(target_id) as HTMLElement;
  if (rootElem === undefined || rootElem === null) {
    console.log("WARNING: Cannot find root elem, not mounting.");
    return;
  }

  const root = ReactDOM.createRoot(rootElem);
  root.render(
    <React.StrictMode>
      <App trace={trace} />
    </React.StrictMode>
  );

  // If you want to start measuring performance in your app, pass a function
  // to log results (for example: reportWebVitals(console.log))
  // or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
  reportWebVitals();
}

mountReact("riscvview-root");

// Allow access even if minified.
(globalThis as any)["riscvviewMountReact"] = mountReact;

export { mountReact };
