"""
This module provides internal functionality related to starting and connecting
to a riscvexec server, using py4j. Refer to `riscv_server.py` instead.
"""

from typing import Deque
from collections import deque
from os import environ
from subprocess import Popen
from logging import getLogger
from py4j.java_gateway import JavaGateway, GatewayParameters, CallbackServerParameters, launch_gateway, JavaClass
from pcrs.settings import RISCVEXEC_PATH, RISCVEXEC_CLASSPATH

RISCV_EXECUTOR_PORT = environ.get('RISCV_EXECUTOR_PORT', 5555)
RISCV_EXECUTOR_HOST = environ.get('RISCV_EXECUTOR_HOST', "")

SHOULD_LOG = False
SHOULD_PRINT_STDOUT_AFTER_SERVER_CLOSE = False
SHOULD_PRINT_STDERR_AFTER_SERVER_CLOSE = False


class RiscVExecutorServerConnection:
    _java_port: int
    _python_port: int
    _gateway: JavaGateway
    _proc: Popen

    # Remark: I'm pretty sure use of deque is thread-safe due to python GIL.
    # TBH I'm not sure what the point of Queue is, but one prefers,
    # you could modify new_riscv_executor_server_connection() to use a
    # queue for storing stdout and stderr.

    # Note these are reversed, i.e. _stdout[0] is the most recent output.
    _stdout: Deque[str]
    # Note these are reversed, i.e. _stderr[0] is the most recent output.
    _stderr: Deque[str]

    _closed: bool

    _executor_server_class: JavaClass

    def __init__(
            self, java_port: int, python_port: int, gateway: JavaGateway,
            proc: Popen, stdout: Deque[str], stderr: Deque[str]):
        """
        Do not construct directly. Use new_riscv_executor_server_connection()
        instead.
        """
        self._java_port = java_port
        self._python_port = python_port
        self._gateway = gateway
        self._proc = proc

        self._stdout = stdout
        self._stderr = stderr

        self._executor_server_class = self._gateway.jvm.pcrs.riscv.ExecutorServer

        self._closed = False

    def _confirm_connection(self):
        return self._executor_server_class.confirmConnection()

    def close(self):
        """
        Closes the Java server and the connection made to it.

        Using any objects derived from this server after the connection is
        closed is disallowed.

        Note: You do not need to manually call this if you are using a `with`
        block.
        """
        if not self._closed:
            self._gateway.shutdown(raise_exception=True)
            self._gateway.close(close_callback_server_connections=True)
            self._proc.terminate()
            self._closed = True

            self._maybe_print_streams_on_close()

    def _maybe_print_streams_on_close(self):
        if SHOULD_PRINT_STDOUT_AFTER_SERVER_CLOSE or SHOULD_PRINT_STDERR_AFTER_SERVER_CLOSE:
            m1 = "=== RiscVExecutorServerConnection close ==="
            m2 = "==========================================="

            print(m1)
            if SHOULD_PRINT_STDOUT_AFTER_SERVER_CLOSE:
                print(f"> stdout:\n{''.join(reversed(self._stdout))}")
            if SHOULD_PRINT_STDERR_AFTER_SERVER_CLOSE:
                print(f"> stderr:\n{''.join(reversed(self._stderr))}")
            print(m2)

    def __enter__(self):
        return self

    def __exit__(self, _type, _value, _traceback):
        self.close()


def new_riscv_executor_server_connection() -> RiscVExecutorServerConnection:
    """
    Creates a new riscv_exec server, and returns a
    `RiscVExecutorServerConnection` which is connected to it.
    """
    java_port: int
    python_port: int
    proc: Popen

    stdout = deque()
    stderr = deque()

    # Refer to:
    # https://www.py4j.org/advanced_topics.html#using-py4j-without-pre-determined-ports-dynamic-port-number
    java_port, proc = launch_gateway(
        classpath=RISCVEXEC_CLASSPATH,
        die_on_exit=True,
        return_proc=True,
        cwd=RISCVEXEC_PATH,
        redirect_stdout=stdout,
        redirect_stderr=stderr)
    gateway = JavaGateway(
        gateway_parameters=GatewayParameters(port=java_port),
        callback_server_parameters=CallbackServerParameters(port=0))
    python_port = gateway.get_callback_server().get_listening_port()
    gateway.java_gateway_server.resetCallbackClient(
        gateway.java_gateway_server.getCallbackClient().getAddress(),
        python_port)

    serv = RiscVExecutorServerConnection(
        java_port, python_port, gateway, proc, stdout, stderr)
    assert serv._confirm_connection()
    return serv


# By default, py4j will send a lot of (useless) information to the logger,
# which Django will happily print to the terminal. Therefore, the user
# might wish to disable this behavior.
if not SHOULD_LOG:
    getLogger("py4j").setLevel(9999999)
