"""
This module provides functionality related to the riscvexec server.

In particular, it allows sending student-submitted RiscV assembly code to the
backend riscvexec server, which can then execute the code using rars.

TODO Implement Testing features, and receive feedback (debugging info,
exceptions, etc.) from riscvexec.
"""

import string
from . import riscv_server_connect
import py4j.java_gateway


class RiscVCodeModel:
    """
    Represents RiscV assembly that has been submitted to the server.
    """
    _java_obj: py4j.java_gateway.JavaObject
    status: str

    STARTED = 'started'
    SUCCESS = 'success'
    FAILURE = 'failure'

    def __init__(self, java_obj: py4j.java_gateway.JavaObject):
        """
        Do not construct directly. Use `RiscVExecutorServer.new_code_model()`
        instead.
        """
        self._java_obj = java_obj
        self.status = RiscVCodeModel.STARTED

    def run(self, max_steps=10_000) -> None:
        self._java_obj.run(max_steps)

        if self._java_obj.hasErrored():
            self.status = RiscVCodeModel.FAILURE
            return RiscVCodeModel.FAILURE
        else:
            self.status = RiscVCodeModel.SUCCESS
            return RiscVCodeModel.SUCCESS

    def get_error_message(self) -> str:
        return self._java_obj.getErrorMessage()

    def get_trace_as_json(self) -> str:
        """
        Returns the (current) trace as a JSON-encoded string.
        """
        # TODO: Check self.status. Throw?
        self._java_obj.makeTrace()
        return self._java_obj.getTraceAsJson()

    def getregistervalue(self, my_in) -> int:
        return self._java_obj.getRegisterValue(my_in)

    def getoutput(self) -> str:
        return self._java_obj.get_output()


class RiscVExecutorServer:
    _connection: riscv_server_connect.RiscVExecutorServerConnection
    _executor_server_instance: py4j.java_gateway.JavaObject

    def __init__(
            self,
            connection: riscv_server_connect.RiscVExecutorServerConnection):
        """
        Do not construct directly. Use new_riscv_executor_server() instead.
        """
        self._connection = connection

        # Creates a `singleton` _executor_server_instance.
        self._executor_server_instance = connection._executor_server_class()

    def __enter__(self):
        return self

    def __exit__(self, type=None, value=None, traceback=None):
        self._connection.__exit__(type, value, traceback)

    def new_code_model(self, asm_code: str, new_stdin: str = "") -> RiscVCodeModel:
        """
        Create a new RiscVCodeModel object, given assembly code `asm_code`.
        """
        code_model = self._executor_server_instance.newCodeModel(
            asm_code, new_stdin)
        return RiscVCodeModel(code_model)


def new_riscv_executor_server():
    """
    Creates a new riscv_exec server, and returns a `RiscVExecutorServer` object
    which is connected to it.
    """
    return RiscVExecutorServer(
        riscv_server_connect.new_riscv_executor_server_connection())
