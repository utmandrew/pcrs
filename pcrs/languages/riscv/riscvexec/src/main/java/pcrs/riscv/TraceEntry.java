package pcrs.riscv;

import java.util.List;

import rars.riscv.hardware.AddressErrorException;
import rars.riscv.hardware.FloatingPointRegisterFile;
import rars.riscv.hardware.Memory;
import rars.riscv.hardware.RegisterFile;

public class TraceEntry {
    public TraceEntry() {
    }

    /**
     * Number of steps that have already been executed. Is 0 if no
     * instructions have been executed yet.
     * 
     * -1 means this information is not available.
     */
    private long steps = -1;
    /**
     * Array of 32 registers (x0 through x31).
     * 
     * null means this information is not available.
     */
    private long[] registers = null;
    /**
     * Array of 32 floating point registers.
     * 
     * null means this information is not available.
     */
    private long[] floatingPointRegisters = null;
    // TODO: control and status registers?
    /**
     * The value of PC (program counter). Note this is the value of PC _after_
     * executing the traced instruction, which usually is NOT the address of
     * the executed instruction.
     * 
     * null means this information is not available.
     */
    private Long programCounter = null;
    /**
     * The value of PC (program counter) for the traced instruction.
     *
     * null means this information is not available. In particular, this will
     * always be null for step zero, since step zero does correspond to the
     * execution of any instructions.
     */
    private Long instructionPC = null;
    /**
     * List of register and memory accesses.
     * 
     * null means this information is not available
     */
    private List<Access> accesses = null;
    /**
     * Information about the statement executed (i.e, the statement at
     * `instructionPc`).
     *
     * null means this information is not available. In particular, this will
     * always be null for step zero, since step zero does not correspond to the
     * execution of any instructions.
     */
    private TraceStatement statement = null;

    protected void captureRegisters() {
        final var rarsRegisters = RegisterFile.getRegisters();
        registers = new long[32];

        for (final var r : rarsRegisters) {
            final var n = r.getNumber();
            final var v = r.getValueNoNotify();
            registers[n] = v;
        }
    }

    protected void captureFloatingPointRegisters() {
        final var rarsFpRegisters = FloatingPointRegisterFile.getRegisters();
        floatingPointRegisters = new long[32];

        for (final var r : rarsFpRegisters) {
            final var n = r.getNumber();
            final var v = r.getValueNoNotify();
            floatingPointRegisters[n] = v;
        }
    }

    protected void captureProgramCounter() {
        programCounter = Long.valueOf(RegisterFile.getProgramCounterRegister().getValueNoNotify());
    }

    /**
     * IMPORTANT NOTE: This must be called _before_ executing the instruction.
     */
    protected void captureInstructionProgramCounter() {
        // Safety check: if programCounter is not null, we are probably calling
        // this function too late.
        if (programCounter != null) {
            throw new RuntimeException(
                    "ERROR - captureInstructionProgramCounter must be called before executing instruction.");
        }

        instructionPC = Long.valueOf(RegisterFile.getProgramCounterRegister().getValueNoNotify());
    }

    /**
     * IMPORTANT NOTE: This must be called after
     * `captureInstructionProgramCounter()`.
     */
    protected void captureStatement(Memory memory) throws AddressErrorException {
        final var rarsStatement = memory.getStatementNoNotify(this.instructionPC.intValue());

        if (rarsStatement == null) {
            System.out.println("captureStatement did capture but stmt was null");
            return;
        }

        statement = new TraceStatement(rarsStatement);
    }

    protected void setSteps(long steps_) {
        steps = steps_;
    }

    protected void captureAccessesAndClearObserver(AccessObserver accessObserver) {
        accesses = accessObserver.getAccesses();
        accessObserver.clearAccesses();
    }

    public sealed interface Access {
        public record ReadMemory(String type, long address, long size, long val) implements Access {

            public static final String TYPE = "ReadMemory";

            public ReadMemory(long address, long size, long val) {
                this(TYPE, address, size, val);
            }
        }

        public record WriteMemory(String type, long address, long size, long val) implements Access {

            public static final String TYPE = "WriteMemory";

            public WriteMemory(long address, long size, long val) {
                this(TYPE, address, size, val);
            }
        }

        public record ReadReg(String type, String name, int number) implements Access {

            public static final String TYPE = "ReadReg";

            public ReadReg(String name, int number) {
                this(TYPE, name, number);
            }
        }

        public record WriteReg(String type, String name, int number) implements Access {

            public static final String TYPE = "WriteReg";

            public WriteReg(String name, int number) {
                this(TYPE, name, number);
            }
        }
    }
}
