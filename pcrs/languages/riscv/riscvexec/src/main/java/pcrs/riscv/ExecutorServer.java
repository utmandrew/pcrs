package pcrs.riscv;

import java.net.Inet4Address;
import java.net.UnknownHostException;

import py4j.GatewayServer;

public final class ExecutorServer {
    public ExecutorServer() {

    }

    public RiscVCodeModel newCodeModel(String assembly, String new_stdin) {
        return new RiscVCodeModel(assembly, new_stdin);
    }

    public static boolean confirmConnection() {
        System.out.println("CONNECTION CONFIRMED");
        return true;
    }

    public static void main(String[] args) {
        if (true)
            throw new RuntimeException(
                    "By default, py4j initializes the server without going through the main method. "
                            + "you probably dont' need to start the server separately. If you have changed this, or if you are "
                            + "simply debugging riscvexec, then comment this out.");

        try {
            String h = System.getenv("RISCV_EXECUTOR_HOST");
            if (h == null || h.isEmpty())
                h = "0.0.0.0";

            String p = System.getenv("RISCV_EXECUTOR_PORT");
            if (p == null || p.isEmpty())
                p = "5555";

            GatewayServer gatewayServer = new GatewayServer.GatewayServerBuilder(new ExecutorServer())
                    .javaAddress(Inet4Address.getByName(h)).javaPort(Integer.parseInt(p))
                    .build();

            gatewayServer.start();

            System.out.printf("Gateway Server Started on %s:%s.\n", h, p);
        } catch (NumberFormatException e) {
            System.err.println("ERROR: Environment variable RISCV_EXECUTOR_PORT needs to be a port.");
        } catch (UnknownHostException e) {
            System.err.println("ERROR: UnknownHostException.");
            throw new RuntimeException(e);
        }
    }
}
