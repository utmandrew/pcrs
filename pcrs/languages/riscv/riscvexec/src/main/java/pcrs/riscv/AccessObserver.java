package pcrs.riscv;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import pcrs.riscv.TraceEntry.Access;
import rars.riscv.hardware.AccessNotice;
import rars.riscv.hardware.MemoryAccessNotice;
import rars.riscv.hardware.RegisterAccessNotice;

public class AccessObserver implements Observer {
    private List<Access> accesses = new ArrayList<>();

    @Override
    public void update(Observable resource, Object genericNotice) {
        if (genericNotice instanceof MemoryAccessNotice notice) {
            if (notice.getAccessType() == AccessNotice.READ) {
                accesses.add(new Access.ReadMemory(notice.getAddress(), notice.getLength(),
                        Integer.toUnsignedLong(notice.getValue())));
            } else if (notice.getAccessType() == AccessNotice.WRITE) {
                accesses.add(new Access.WriteMemory(notice.getAddress(), notice.getLength(),
                        Integer.toUnsignedLong(notice.getValue())));
            } else {
                throw new RuntimeException("Should be Unreachable");
            }
        } else if (genericNotice instanceof RegisterAccessNotice notice) {
            if (notice.getAccessType() == AccessNotice.READ) {
                accesses.add(new Access.ReadReg(notice.getRegisterName(), notice.getRegisterNumber()));
            } else if (notice.getAccessType() == AccessNotice.WRITE) {
                accesses.add(new Access.WriteReg(notice.getRegisterName(), notice.getRegisterNumber()));
            } else {
                throw new RuntimeException("Should be Unreachable");
            }
        }
    }

    public void clearAccesses() {
        accesses.clear();
    }

    public List<Access> getAccesses() {
        return List.copyOf(accesses);
    }

}
