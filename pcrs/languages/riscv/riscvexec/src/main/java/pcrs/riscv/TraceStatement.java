package pcrs.riscv;

import rars.ProgramStatement;

public final class TraceStatement {
    final String sourceLine;
    final int sourceLineNumber;
    final String actualAsm;
    final int address;

    public TraceStatement(ProgramStatement rarsStatement) {
        sourceLine = rarsStatement.getSource();
        sourceLineNumber = rarsStatement.getSourceLine();
        address = rarsStatement.getAddress();
        actualAsm = rarsStatement.getBasicAssemblyStatement();
    }

}
