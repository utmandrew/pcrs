package pcrs.riscv;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

public class Trace {
    private static final Gson gson = new Gson();

    private String sourceFile;
    private List<TraceEntry> traceEntries;

    public Trace() {
        traceEntries = new ArrayList<>();
    }

    protected void setSourceFile(String source) {
        sourceFile = source;
    }

    protected void addTraceEntry(TraceEntry e) {
        traceEntries.add(e);
    }

    public String toJson() {
        return gson.toJson(this, this.getClass());
    }
}
