package pcrs.riscv;

import java.util.List;
import rars.AssemblyException;
import rars.SimulationException;
import rars.api.Options;
import rars.api.Program;
import rars.riscv.SyscallLoader;
import rars.riscv.hardware.AddressErrorException;
import rars.riscv.hardware.ControlAndStatusRegisterFile;
import rars.riscv.hardware.FloatingPointRegisterFile;
import rars.riscv.hardware.RegisterFile;
import rars.simulator.Simulator;
import rars.Globals;

public class RiscVCodeModel {
    // Refer to "Syscall.proprieties" file for a list of syscalls.
    //
    // Invoking any syscalls not present here will cause a "invalid or
    // unimplemented syscall service" error in student code.
    final static List<String> allowedSyscalls = List.of(
            "Read", "PrintString", "PrintInt", "PrintFloat", "PrintDouble","Exit2");

    final String assembly;
    final Trace trace = new Trace();
    final String stdin;

    boolean hasErrored = false;
    String errorMessage = null;
    Program my_p;

    String traceCache;

    // TODO move
    public static String getRarsError(Exception e) {
        if (e instanceof AssemblyException) {
            return ((AssemblyException) e).errors().generateErrorReport();
        }
        if (e instanceof SimulationException) {
            return ((SimulationException) e).error().generateReport();
        }
        if (e instanceof AddressErrorException) {
            return ((AddressErrorException) e).getMessage();
        }
        throw new RuntimeException("Unknown error type " + e.getClass().toString());
    }

    // Note: Temporarily, we have two separate methods: One for making the
    // trace, and another for returning it. This allows us to benchmark the
    // performance of these separately. Eventually, we will likely want to
    // merge these into a single method.

    public void makeTrace() {
        traceCache = trace.toJson();
    }

    public String getTraceAsJson() {
        return traceCache;
    }

    public boolean hasErrored() {
        return hasErrored;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public RiscVCodeModel(String assembly, String new_stdin) {
        this.assembly = assembly;
        this.stdin = new_stdin;
    }

    public int getRegisterValue(String input) {
        return this.my_p.getRegisterValue(input);
    }

    public String get_output() {
        return Globals.output;
    }

    public void removeDisallowedSyscalls() {
        // Slightly hacky solution: we mutate the syscall list.
        var syscallList = SyscallLoader.getSyscallList();
        syscallList.removeIf((s) -> {
            boolean remove = !allowedSyscalls.contains(s.getName());
            if (remove) {
                System.out.printf("Removing syscall %s\n", s.getName());
            } else {
                System.out.printf("Keeping syscall %s\n", s.getName());
            }
            return remove;
        });
    }

    public boolean run(int maxSteps) {
        final var accessObserver = new AccessObserver();
        int steps = 0;

        String stdin = this.stdin;

        System.out.println("Running");
        System.out.println(Thread.currentThread().getId());

        Options options = new Options();
        options.maxSteps = 1;

        Program program = new Program(options);

        removeDisallowedSyscalls();

        try {
            program.assembleString(assembly);
            program.setup(null, stdin);

            program.getMemory().addObserver(accessObserver);
            RegisterFile.addRegistersObserver(accessObserver);
            FloatingPointRegisterFile.addRegistersObserver(accessObserver);
            ControlAndStatusRegisterFile.addRegistersObserver(accessObserver);
            this.my_p = program;
            trace.setSourceFile(assembly);

            {
                final var startEntry = new TraceEntry();
                startEntry.setSteps(steps);
                startEntry.captureRegisters();
                startEntry.captureFloatingPointRegisters();
                startEntry.captureProgramCounter();
                startEntry.captureAccessesAndClearObserver(accessObserver);
                trace.addTraceEntry(startEntry);
            }

            Simulator.Reason reason;
            while (true) {
                // Observe that captureInstructionProgramCounter is called
                // _before_ executing the instruction. This is critical!
                final var entry = new TraceEntry();
                entry.captureInstructionProgramCounter();

                reason = program.simulate();
                if (reason != Simulator.Reason.MAX_STEPS || steps + 1 >= maxSteps)
                    break;
                steps++;

                entry.setSteps(steps);
                entry.captureRegisters();
                entry.captureFloatingPointRegisters();
                entry.captureProgramCounter();
                entry.captureAccessesAndClearObserver(accessObserver);
                entry.captureStatement(program.getMemory());

                trace.addTraceEntry(entry);

            }

            return true;
        } catch (AssemblyException | SimulationException | AddressErrorException e) {

            hasErrored = true;
            errorMessage = e.getClass().toString() + "\n\n" + getRarsError(e);

            // TODO: handle exception
            System.out.print("\n\nerror class:\n");
            System.out.println(e.getClass().toString());

            System.out.print("\n\nrars error message:\n");
            System.out.println(getRarsError(e));

            System.out.print("\n\njava error message: ");
            System.out.println(e.getMessage());
            e.printStackTrace();

        } catch (Exception e) {

            hasErrored = true;
            errorMessage = e.getClass().toString() + "\n\n" + e.getMessage();

            System.out.print("\n\nerror class:\n");
            System.out.println(e.getClass().toString());

            System.out.print("\n\nerror message:\n");
            System.out.println(e.getMessage());

            e.printStackTrace();

        } finally {
            System.out.printf("Simulated %d instructions\n", steps);
            System.out.printf("Contents of a0=%d\n", program.getRegisterValue("a0"));
            // System.out.printf("Contents of a7=%d\n", program.getRegisterValue("a7"));

        }

        return false;
    }

}
