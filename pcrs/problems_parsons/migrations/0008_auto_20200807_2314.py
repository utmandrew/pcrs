# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2020-08-08 03:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('problems_parsons', '0007_auto_20200807_2307'),
    ]

    operations = [
        migrations.AddField(
            model_name='submission',
            name='incorrect_lines',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='submission',
            name='reason_incorrect',
            field=models.IntegerField(default=-1),
        ),
    ]
