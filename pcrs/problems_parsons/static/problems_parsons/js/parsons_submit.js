function getParsonsHistory(qid){
    /**
     * get all previous submissions for the problem given its main div id field "mc_id"
     */
    const postParams = { "csrfmiddlewaretoken": getCookie("csrftoken") };

    $.post(root+'/problems/parsons/'+qid.split("-")[1]+'/history',
    postParams,
    function(data){
        $('#'+qid).find('#history_accordion').html("");
        show_parsons_history(data, qid);
        $('#history_window_' + qid).modal('show');
    },
    'json')
    .fail(function(jqXHR, textStatus, errorThrown) { console.log(textStatus); });
}

function show_parsons_history(data, div_id){
    /**
     * add all the previous submissions one at a time to the history inside the given div "div_id"
     */

    for (let x = 0; x < data.length; x++){
        add_parsons_history_entry(data[x], div_id, 0);
    }
}

function create_parsons_timestamp(datetime){
    /**
     * Convert Django time to PCRS display time for history
     */

    const month_names = ["January","February","March","April","May","June",
                    "July","August","September","October","November","December"];
    let day = datetime.getDate();
    let month = month_names[datetime.getMonth()];
    let year = datetime.getFullYear();
    let hour = datetime.getHours();
    let minute = datetime.getMinutes();
    let cycle = "";

    if (String(minute).length == 1){
        minute = "0" + minute
    }

    if (hour > 12){
        hour -= 12;
        cycle = "p.m.";
    }
    else{
        cycle = "a.m.";
    }

    const formated_datetime = month + " " + day + ", "+year + ", " + hour+":"+minute+" "+cycle
    return formated_datetime;
}

function add_parsons_history_entry(data, div_id, flag){
    /**
     * Add a given previous submission "data" to the history of a given "div_id"
     * "flag" 0 appends anf "flag" 1 prepends
     */
    let sub_time = new Date(data['sub_time']);
    sub_time = create_parsons_timestamp(sub_time);

    let panel_class = "pcrs-panel-default";

    if (data['past_dead_line']){
        panel_class = "pcrs-panel-warning";
        sub_time = sub_time + " Submitted after the deadline";
    }

    star_text = "";

    if (data['best'] && !data['past_dead_line']){
        panel_class = "pcrs-panel-star";
        star_text = '<icon style="font-size:1.2em" class="star-icon"> </icon>';
        $('#'+div_id).find('#history_accordion').find(".star-icon").removeClass("star-icon");
        $('#'+div_id).find('#history_accordion').find(".pcrs-panel-star")
            .addClass("pcrs-panel-default").removeClass("pcrs-panel-star");
    }

    const entry = $('<div/>',{class:panel_class});
    const header1 = $('<div/>',{class:"pcrs-panel-heading"});
    const header2 = $('<h4/>', {class:"pcrs-panel-title"});
    const header4 = $('<td/>', {html:"<span style='float:right;'> " + star_text + " "
                                    + "<sup style='font-size:0.9em'>" + data['score'] + "</sup>"
                                    + " / "
                                    + "<sub style='font-size:0.9em'>"+ data['out_of']+" </sub>"
                                    + "</span>"});
    const header3 = $('<a/>', {'data-toggle':"collapse",
                            'data-parent':"#history_accordion",
                            href:"#collapse_"+data['sub_pk'],
                            html:sub_time + header4.html()});

    const cont1 = $('<div/>', {class:"pcrs-panel-collapse collapse",
                                id:"collapse_" + data['sub_pk']});

    const cont2 = $('<div/>', {id:"Parsons_answer_"
                                    + data['problem_pk']
                                    + "_"
                                    + data['sub_pk']});
                                    
    const sortable_id = `sortable_${data['sub_pk']}`;
    const trash_id = `sortableTrash_${data['sub_pk']}`;
    cont2.append($('<div/>', {id: trash_id, class:"sortable-code", style:"width: 50%"}));
    cont2.append($('<div/>', {style: 'clear:both;'}));
    cont2.append($('<div/>', {id: 'jsparson'}));
    header2.append(header3);
    header1.append(header2);
    
    entry.append(header1);
    cont1.append(cont2);
    entry.append(cont1);

    if (flag == 0){
        $('#'+div_id).find('#history_accordion').append(entry);
    }
    else{
        $('#'+div_id).find('#history_accordion').prepend(entry);
    }
    const payload = {'sortableId': sortable_id, 'trashId': trash_id};
    const parson = new ParsonsWidget(payload);
    parson.init(data['submission']);
    parson.options.trash_label = null;
    parson.displayHTML();
}