import json
import logging
import datetime

from django.urls import reverse
from django.http import HttpResponse
from django.shortcuts import redirect
from django.utils.timezone import localtime, utc
from django.views.generic import CreateView, FormView, View
from django.views.generic.detail import SingleObjectMixin

from problems_parsons.models import Problem, Submission, TestCase
from problems_parsons.forms import SubmissionForm

import problems.views
from users.views import UserViewMixin
from users.views_mixins import ProtectedViewMixin, CourseStaffViewMixin


class ProblemUpdateView(problems.views.ProblemUpdateView):
    """
    Update a problem.
    """
    template_name = 'problems/problem_form.html'

    def correctify_max_score(self):
        """
        This method corrects the max score issue with the
        testing of student submission. This method will assign the
        correct max_score to the problem. Note that this is not optimal
        as we need to query the database every time we need to correct.
        """
        # If line comparison AND unit tests are being used to evaluate
        if str(self.object.evaluation_type[0]) == str(self.object.EvaluationChoices.ALL_METHODS):
            # Num. of unit tests + 1 for the line comparison
            num_tests = self.object.get_num_tests()
            self.object.max_score = num_tests + 1

        # If line comparison is being used to evaluate
        if str(self.object.evaluation_type[0]) == str(self.object.EvaluationChoices.LINE_COMPARISON):
            # Only 1 for the line comparison
            self.object.max_score = 1

        # If unit tests are being used to evaluate
        if str(self.object.evaluation_type[0]) == str(self.object.EvaluationChoices.UNIT_TESTS):
            # Only the number of unit tests
            num_tests = self.object.get_num_tests()
            self.object.max_score = num_tests

        self.object.save()

    def get_success_url(self):
        self.correctify_max_score() # correct the max score
        if 'attempt' in self.request.POST:
            return '{}/submit'.format(self.object.get_absolute_url())
        else:
            return self.object.get_absolute_url()




class ProblemCloneView(problems.views.ProblemCloneView):
    def form_valid(self, form):
        new_problem = form.save()
        return redirect(new_problem.get_absolute_url())


class ProblemCreateRedirectView(CourseStaffViewMixin, CreateView):
    model = Problem

    def get_success_url(self):
        return reverse('parsons_update',
                       kwargs={'pk': self.object.pk})


class SubmissionViewMixin(problems.views.SubmissionViewMixin, FormView):
    model = Submission
    form_class = SubmissionForm
    template_name = 'problems_parsons/submission.html'

    def record_submission(self, request):
        """
        Record the submission and return the results of running the solution code.
        """
        problem = self.get_problem()
        self.submission = self.model.objects.create(problem=problem,
                              user=request.user, section=self.get_section())
        return self.submission.set_score(request.POST['submission'])


class SubmissionView(ProtectedViewMixin, SubmissionViewMixin,
                     SingleObjectMixin, FormView, UserViewMixin):
    object = None

    def post(self, request, *args, **kwargs):
        """
        Record the submission and redisplay the problem submission page,
        with latest submission prefilled.
        """
        form = self.get_form(self.get_form_class())
        results = self.record_submission(request)
        return self.render_to_response(self.get_context_data(form=form, results=results,
                                                             submission=self.submission))


class SubmissionAsyncView(SubmissionViewMixin, SingleObjectMixin, View,
                          UserViewMixin):
    def post(self, request, *args, **kwargs):
        try:
            results = self.record_submission(request)
        except AttributeError:       # Anonymous user
            return HttpResponse(json.dumps({
                                'error_msg': "Your session has expired. Please reload the page (to re-authenticate) before submitting again.",
                                'score': 0,
                                'max_score': 1,
                                'sub_pk': None,
                                'best': False,
                                'past_dead_line': False,
                                }), content_type='application/json')

        problem = self.get_problem()
        user, section = self.get_user(), self.get_section()
        try:
            deadline = problem.challenge.quest.sectionquest_set.get(section=section).due_on
        except:   # content.models.DoesNotExist
            deadline = None

        logger = logging.getLogger('activity.logging')
        logger.info(str(localtime(self.submission.timestamp)) + " | " +
                    str(user) + " | Submit " +
                    str(problem.get_problem_type_name()) + " " +
                    str(problem.pk))

        return HttpResponse(json.dumps({
            'submission': self.submission.submission,
            'score': self.submission.score,
            'max_score': problem.max_score,
            'best': self.submission.has_best_score,
            'sub_pk': self.submission.pk,
            'past_dead_line': deadline and self.submission.timestamp > deadline,
            'results': results
            }), content_type='application/json')


class SubmissionHistoryAsyncView(SubmissionViewMixin, SingleObjectMixin,
                                 UserViewMixin, View):
    def post(self, request, *args, **kwargs):
        problem = self.get_problem()
        user, section = self.get_user(), self.get_section()
        try:
            deadline = problem.challenge.quest.sectionquest_set\
                .get(section=section).due_on
        except Exception:
            deadline = False
        try:
            best_score = self.model.objects\
                .filter(user=user, problem=problem, has_best_score=True).latest("id").score
        except self.model.DoesNotExist:
            best_score = -1

        data = self.model.objects\
            .prefetch_related('testrun_set', 'testrun_set__testcase')\
            .filter(user=user, problem=problem)

        returnable = []
        for sub in data:
            returnable.append({
                'sub_time': sub.timestamp.isoformat(),
                'submission': sub.submission,
                'reason_incorrect': sub.reason_incorrect,
                'incorrect_inlines': sub.incorrect_lines,
                'score': sub.score,
                'out_of': problem.max_score,
                'best': sub.has_best_score and \
                        ((not deadline) or sub.timestamp < deadline),
                'past_dead_line': deadline and sub.timestamp > deadline,
                'problem_pk': problem.pk,
                'sub_pk': sub.pk,
                'tests': [testrun.get_history()
                          for testrun in sub.testrun_set.all()]
            })

        return HttpResponse(json.dumps(returnable), content_type='application/json')
