import Levenshtein as lev
from fuzzywuzzy import fuzz
import textdistance
import statistics as stats
from problems_rdb.sqam.sqam_utils import (sort_list_of_tuples, get_extra_rows, get_missing_rows,
                                          apply_distance_metric_on_tuples)


LIMIT_OF_CHARACTER = 0
MAX_MARKS_THRESHOLD = 0.90
MAX_MARKS_THRESHOLD_BINARY = 0.95


class String_Similarity_Grader():
    """
    String Similarity Grader uses the following libraries to calculate the string similarity of the
    submission to the solution. This similarity is used to provide partial marks for submissions that
    are not exactly correct. 
    ### Python Levenshtein
    https://rawgit.com/ztane/python-Levenshtein/master/docs/Levenshtein.html#Levenshtein-ratio
    ### Python FuzzyWuzzy
    https://rawgit.com/ztane/python-Levenshtein/master/docs/Levenshtein.html#Levenshtein-ratio
    ### Text Distance
    https://pypi.org/project/textdistance/
    """

    def __init__(self, questions = None, solution_results = None, kind = 'partial'):
        if questions == None:
            questions = []
            solution_results = []
        self.questions = questions
        self.solution_results = solution_results
        self.kind = kind

    def get_marks_for_question(self, student_ans, solution, max_marks_for_question):
        """
        Grade the students answer using string similarity metrics
        @param student_ans: Students output for a single query
        @param solution: Correct output for query
        @param max_marks_for_question: Max possible marks for query
        @return: The student's mark
        """
        ratio = self.get_distance(student_ans, solution)
        marks = round(max_marks_for_question * ratio)
        marks = max_marks_for_question if marks > max_marks_for_question else marks
        if self.kind == "binary":
            marks = max_marks_for_question if ratio > MAX_MARKS_THRESHOLD_BINARY else 0
        else:
            marks = max_marks_for_question if ratio > MAX_MARKS_THRESHOLD else marks
        return marks, ratio

    def get_greatest_average_similarity(self, list_of_list_of_all_string_distances):
        max_score = 0
        all_scores = []
        for list_of_string_distances in list_of_list_of_all_string_distances:
            if isinstance(list_of_string_distances, list) and len(list_of_string_distances) > 0:
                score = sum(list_of_string_distances) / \
                    len(list_of_string_distances)
                score = score / 100 if score > 1 else score
                max_score = score if score > max_score else max_score
                all_scores.append(score)
        if self.kind == "binary":
            return stats.mean(all_scores)
        else:
            return max_score

    def get_distance(self, student_results, solution_results):
        student_results_lst = sort_list_of_tuples(
            student_results[1:])
        solution_results_lst = sort_list_of_tuples(
            solution_results[1:])
        ratcliff_dist, jaro_winkler_dist, fuzzy_token_dist, levenshtein_dist = [], [], [], []
        for item1, item2 in zip(student_results_lst, solution_results_lst):
            levenshtein_dist.append(lev.seqratio(item1, item2))
            fuzzy_token_dist.append(apply_distance_metric_on_tuples(
                item1, item2, fuzz.token_sort_ratio))
            jaro_winkler_dist.append(apply_distance_metric_on_tuples(
                item1, item2, textdistance.jaro_winkler))
            ratcliff_dist.append(apply_distance_metric_on_tuples(
                item1, item2, textdistance.ratcliff_obershelp))
        return self.get_greatest_average_similarity([ratcliff_dist, jaro_winkler_dist, fuzzy_token_dist, levenshtein_dist])
