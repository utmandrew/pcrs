from operator import itemgetter


def sort_list_of_tuples(lst):
    ret = []
    try:
        lst.sort(key=itemgetter(0))
    except Exception as e:
        pass
    for tup in lst:
        tup = map(str, tup)
        tup = sorted(tup)
        ret.append(tuple(tup))
    ret.sort(key=itemgetter(0))
    return ret


def get_extra_rows(answer, solutions):
    ans_set = set(answer)
    sol_set = set(solutions)
    diff = ans_set.difference(sol_set)
    if not diff or diff == {('', '')}:
        return None
    return diff


def get_missing_rows(answer, solutions):
    ans_set = set(answer)
    sol_set = set(solutions)
    diff = sol_set.difference(ans_set)
    if not diff or diff == {('', '')}:
        return None
    return diff


def apply_distance_metric_on_tuples(tup1, tup2, func):
    vals = []
    newtup1 = list(tup1)
    newtup2 = list(tup2)
    for item1, item2 in zip(newtup1, newtup2):
        val = func(item1, item2)
        vals.append(val)
    return sum(vals) / len(vals) if len(vals) > 0 else 0
