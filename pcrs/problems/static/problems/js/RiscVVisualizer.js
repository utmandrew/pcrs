function RiscVVisualizer() {
    Visualizer.call(this);
    this.language = 'riscv';
}
RiscVVisualizer.prototype = Object.create(Visualizer.prototype);
RiscVVisualizer.prototype.constructor = RiscVVisualizer;

/**
 * @override
 */
RiscVVisualizer.prototype._showVisualizerWithData = function (data) {
    if (data.exception) {
        alert("Failed to generate RiscV Trace: " + data.exception)
        console.log(data)
    } else {
        $('#visualizerModal').modal('show');
        // We can do this since the iframe is served by pcrs in accordance w/ same origin policy.
        document.getElementById("riscvview-iframe").contentWindow['riscvviewMountReact']("riscvview-root", data);
    }
}

// TODO: If needed, perhaps override _generatePostParams.