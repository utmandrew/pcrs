# Generated by Django 3.2.4 on 2022-05-27 21:56

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('problems', '0018_alter_fileupload_lifespan'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fileupload',
            name='lifespan',
            field=models.DateTimeField(default=datetime.datetime(2022, 5, 28, 21, 56, 28, 696862, tzinfo=utc), null=True),
        ),
    ]
