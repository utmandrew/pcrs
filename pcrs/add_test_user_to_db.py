import psycopg2
from psycopg2.errors import UniqueViolation
from pcrs.settings import DATABASES

try:
    db_settings = DATABASES['default']

    # print(db_settings)

    conn = psycopg2.connect(
        dbname=db_settings['NAME'],
        user=db_settings['USER'],
        password=db_settings['PASSWORD'],
        host=db_settings['HOST'],
        port=db_settings['PORT'])

    cur = conn.cursor()

    cur.execute(
        """
        INSERT INTO users_pcrsuser (last_login, username, section_id, code_style, is_student, is_ta, is_instructor, is_active, is_admin, is_staff, use_simpleui)
        VALUES (CURRENT_TIMESTAMP, 'test', 'master', 'eclipse', False, False, True, True, True, True, False);
        """
    )

    conn.commit()
    cur.close()
    conn.close()
except UniqueViolation:
    pass
