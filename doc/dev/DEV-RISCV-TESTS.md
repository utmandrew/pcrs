# Running RISCV Tests

(Note the database must be accessible to execute any tests.)

First, cd to the `pcrs/` subdirectory. Then, execute each of the following:

- `python3 manage.py test languages.riscv`

- `python3 manage.py test problems_riscv`
