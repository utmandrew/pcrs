# Guide to RiscV tests on PCRS

This is a guide on how to set up test cases for RiscV on PCRS.

This guide will go over the types of test cases as well as 

## How to set up the different types of test cases
So there are two types of test cases for RiscV.

The first is a test to check a register's value at the end of execution. To set this test up in the pre_code section enter ```"register"```. Then in test input you put the register you want to check as well as the expected value in expected value. 

The other is a test to check the output. To set this test up in the pre_code section enter ```"output"```. The test input section here will not actually be used in the test iself, it is a way for students to see what is being sent in as stdin. Make sure to specify the desired output in the expected output section

For both test it is possible to send over custom stdin. To do this add a space after ```"register"``` or ```"output"``` and enter what you want students to read. It is important to note that strings are null terminated in RiscV so you can choose a delimiter of your choice as it will not be read. Below are some examples for the different types of stdin you can specify. 

# Examples 

### For a single input of length 1

```output 2p``` This test case will compare a students output with what was expected and will provide "2" as input. "p" is not read.

```register a.``` This test case will check a students register that was specified in test input and will provide "a" as input. "." is not read.

### For a single input of length 3

```output 135g``` This test case will compare a students output with what was expected and will provide "135" as input. "g" is not read.

```register abc!``` This test case will check a students register that was specified in test input and will provide "abc" as input. "!" is not read.

### For multiple inputs of the same length

```output 257p344r649*``` This test case will compare a students output with what was expected and will provide "257", "344", and "649" as input. "p", "r", and "*" are not read.

```register hello.world?``` This test case will check a students register that was specified in test input and will provide "hello" and "world: as input. "." and "?" are not read.

### For multiple inputs of varying lengths 

```output 235p711131719A5.``` This test case will compare a students output with what was expected and will provide "235", "711131719" and "5" as input. "p", "A" and "." are not read.

```register abc.hello!worlds$``` This test case will check a students register that was specified in test input and will provide "abc", "hello", and "world" as input. ".", "!", and "$" are not read. 

### A note on the delimiters

The delimiter characters are used to seperate inputs and will not be read unless students were to read too far. The characters used to seperate inputs do not really matter as long as they are made up of a single character. Characters like the newline character "\n" or tab "\t" would not work as the simulator would read them as more than 1 character. Additonally the space character " " will work as long as it is not at the end of the input. For example in the single input of length 1 example a space will not work as it will be removed. However, if a space was used in the multiple inputs example as the delimiter for the first or second input then there would be no issue. 

## How to include and exclude certain syscalls
By default all syscalls will be excluded. Syscalls can be added by adding them to allowed syscalls list in RiscVCodeModel.java which can be found in `pcrs/languages/riscv/riscvexec/src/main/java/pcrs/riscv`, consult the Syscall.properties file which can be found in `riscvexec/vendor/rars/src`. Add the name of the syscall to the list for example this list could look like `final static List<String> allowedSyscalls = List.of("Read", "PrintString", "PrintInt", "PrintFloat", "PrintDouble", "Exit2");`.

## Limitations
Currently the only syscall that can read custom standard input is the basic read call as the other syscalls require further modification of rars. These syscalls prompt the console for input. A workaround to this would be to have students use the basic read call and parse the input into their desired type.

It is not currently possible to perform memory related tests. A workaround to this would be to get students to print what they have stored in memory.