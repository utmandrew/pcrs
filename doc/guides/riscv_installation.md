# Installing PCRS - RiscV

This is a guide for installing PCRS and setting it up for using RiscV.

This guide will not go over the entire installation process, but only the
additional steps required to set up support for RiscV. Refer to
https://mcs.utm.utoronto.ca/~pcrs/pcrs/installation.shtml for a guide on
installing PCRS.

Note: You can perform all steps below _after_ installing PCRS. Even building
can be done after installing and testing non-riscv related features, as PCRS
only uses these dependencies if riscv features are actually utilized.

## Dependencies

Besides all the dependencies for a traditional PCRS install, you will also need
the following build-time and run-time dependencies.

To build, you will need:

- Java 17, and a version of maven that supports Java 17 (we tested with 3.8.6.
  Note maven 3.6.3 will not work).
- Node.js / NPM (tested with v18.4.0 and 8.12.1, respectively)
- All the "traditional" PCRS dependencies.

Maven will be used to build `riscvexec` and node will be used to build
`riscvview`.

To run, you will need:

- Java 17
- All the "traditional" PCRS dependencies. Make sure to use a python environment
  with all the requirements from `requirements.txt`.

Maven and node are not necessary at runtime. Additionally, it is possible to
build the dependencies `riscvexec` and `riscvview` on a separate machine and
copy the artifacts.

## Building

1. First, build `riscvexec`, as follows:

   Ensure that when you cloned the PCRS git repo, you cloned all git submodules.
   In particular, `rars` (located in `./pcrs/languages/riscv/riscvexec/vendor`)
   is necessary. Note that (for now) we use a fork of `rars`.

   Build riscvexec with maven:

   ```sh
   cd ./pcrs/languages/riscv/riscvexec
   mvn package -f ./pom.xml
   ```

   In particular, this will produce
   `./pcrs/languages/riscv/riscvexec/target/riscvexec-1.0-SNAPSHOT.jar`.

   (Remark: If installing a sufficiently recent version of maven on the
   production server is challenging, you may also perform the build on a
   different computer, and copy over the built files to the appropriate
   directory. By default, PCRS expects the folder structure to be as above, but
   if necessary, you may override the variables `RISCVEXEC_PATH` and
   `RISCVEXEC_CLASSPATH` in your `settings_local.py` file.)

2. The next step is to build `riscvview`, the tracing frontend:

   ```sh
   cd ./pcrs/languages/riscv/riscvview
   npm install
   ./build.sh
   ```

   `riscvview` is served as static files (hence why node is unnecessary for
   running), so you must also copy the built files to the static directory.

   If you are using `pcrs/static` as your static directory, the easiest way to
   do this is to run

   ```sh
   # Use rm if directory already exists
   # rm -rf "static/riscvview"
   cp -r "pcrs/languages/riscv/riscvview/build" "static/riscvview"
   ```

   (Note that the script `./copy-riscvview.sh` does exactly that)

   If successful, you should have a directory `./static/riscvview/` containing
   an index file and a `static` subdirectory.

   If you are collecting your static files to a different directory, make sure
   to copy the build files to the appropriate location.

Make sure to configure `INSTALLED_PROBLEM_APPS` to enable `problems_riscv`.

## Running

Simply run PCRS as usual (ensure you are running java17 or above). Make sure
your environment provides support for running java files using `py4j` ---
installing dependencies using `pip install -r requirements.txt` should suffice.
