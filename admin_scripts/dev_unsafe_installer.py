#!/bin/python3
import subprocess
import sys
import os
import pwd
import secrets

print("""
before/after running this, make sure to setup virtualenv 
if desired and install any requirements with 
(sudo) pip3 install - r requirements.txt
""")


print("NOTE. THIS SCRIPT WILL INSTALL PCRS WITH DEFAULTS JUST FOR DEVELOPMENT")
print("UNDER NO CIRCUMSTANCES SHOULD THIS BE USED IN PRODUCTION (it is INSECURE)")
print("Type y to acknowledge")


if input().casefold() != 'y'.casefold():
    sys.exit(1)

PCRSPlugins = [
    ["Python", "'problems_python': 'Python',"],
    ["C", "'problems_c': 'C',"],
    ["Java", "'problems_java': 'Java',"],
    ["RiscV", "'problems_riscv': 'RiscV',"],
    ["RDB???", "'problems_rdb': '',"],
    ["SQL", "'problems_sql': 'SQL',"],
    ["Relational Algebra", "'problems_ra': 'Relational Algebra',"],
    ["R", "'problems_r': 'R',"],
    ["Multiple Choice", "'problems_multiple_choice': '',"],
    ["Short Answer", "'problems_short_answer': '',"],
    ["Timed", "'problems_timed': '',"],
    ["Rating", "'problems_rating': '',"],
]

if ("proc" not in sys.path[0] and "dev" not in sys.path[0]):
    os.chdir(sys.path[0])
else:
    pathing = input("Enter the path for the source files: ")
    try:
        os.chdir(pathing.replace("~", os.environ['HOME']))
        print(os.getcwd())
    except FileNotFoundError:
        print("An invalid folder was given. Halting.")
        sys.exit(1)

print("System Information\n")
# Print some general info, if they don't have lsb_release
# installed, its not a huge deal.
try:
    result = subprocess.Popen(["lsb_release", "-a"], stdout=subprocess.PIPE)
    out, err = result.communicate()
    print(bytes.decode(out, "UTF-8"))
except FileNotFoundError:
    pass

# Look into their python version...
if sys.version_info[0] == 3 and sys.version_info[1] == 8:
    print("Python Ver:\t3.8." + str(sys.version_info[2]))
else:
    print("\nPython 3.8.X is RECOMMENDED for this software.")
    print("Type y to proceed anyways!")
    if input().casefold() != 'y'.casefold():
        sys.exit(1)

# Look into their postgres version
try:
    result = subprocess.Popen(["psql", "--version"], stdout=subprocess.PIPE)
    out, err = result.communicate()
    out = bytes.decode(out, "UTF-8")
    postgresVer = out.split(" ")[2].split(".")
    if postgresVer[0] == "10" and postgresVer[1] == "14":
        print("Postgres Ver:\t" + ".".join(postgresVer).rstrip("\n"))
    else:
        print("Postgres Ver:\t" +
              ".".join(postgresVer).rstrip("\n") + " (NOT TESTED)")
except FileNotFoundError:
    print("Postgres 10.14.X is required for this software.")
    sys.exit(1)

# GCC is next!
try:
    result = subprocess.Popen(["gcc", "--version"], stdout=subprocess.PIPE)
    out, err = result.communicate()
    out = bytes.decode(out, "UTF-8")
    GCCVer = out.split(" ")[2].split(".")
    if GCCVer[0] == "7" and GCCVer[1] == "5":
        print("GCC Ver:\t" + ".".join(GCCVer))
    else:
        print("GCC Ver:\t" + ".".join(GCCVer) + " (NOT TESTED)")
except FileNotFoundError:
    print("GCC 7.5 is required for C PCRS.")

# A touch of java
try:
    # Because java prints this to stderr.....
    result = subprocess.Popen(["javac", "-version"], stderr=subprocess.PIPE)
    out, err = result.communicate()
    err = bytes.decode(err, "UTF-8")
    JavaVer = err.split(" ")[1].split(".")
    if JavaVer[0] == "11" and JavaVer[1] == "0":
        print("Java JDK Ver:\t" + ".".join(JavaVer).rstrip("\n"))
    else:
        print("Java JDK Ver:\t" + ".".join(JavaVer).rstrip("\n") + " (NOT TESTED)")
except FileNotFoundError:
    print("Java JDK is required for Java PCRS.")

# Now that we know some stuff about the enviroment,
# Lets start to customize the install.
wsgiUser = input("Enter the user which will run PCRS: ")
prefix = ""

print("We're going with Django runserver (not recomended in production)")

clone = input("Clone git repo? Type n if you already did this. (Y/n)")
if clone.lower() == "y" or clone == "":
    try:
        result = subprocess.Popen(
            ["git clone https://bitbucket.org/utmandrew/pcrs.git"], shell=True)
        result.wait()
        os.chdir("pcrs/pcrs")
    except FileNotFoundError:
        path("GIT is unavaliable.")
        sys.exit(1)
else:
    try:
        print("Trying to find pcrs location...")
        pcrsPath = "../pcrs"
        os.chdir("../pcrs")
    except FileNotFoundError:
        print("Could not find pcrs location.")
        print("Please indicate where pcrs is located")
        print("To be clear, this should be the root directory of the git repo: ")
        pcrsPath = input()
        os.chdir(f"{pcrsPath}/pcrs")
        print("")

assert os.path.exists("pcrs/settings.py")

print("Preparing Config")
safeExecEnabled = False
sqlEnabled = False
rdbEnabled = False
userConfig = ""

userConfig += """INSTALLED_PROBLEM_APPS = {
"""
for plugin in PCRSPlugins:
    enable = input("Enable " + plugin[0] + "? (Y/n): ")
    if enable.lower() == "y" or enable == "":
        userConfig += plugin[1] + "\n"
        if plugin[0] == "C":
            safeExecEnabled = True
        elif plugin[0] == "SQL":
            rdbEnabled = sqlEnabled = True
        elif plugin[0] == "Relational Algebra":
            rdbEnabled = True
if rdbEnabled:
    userConfig += "'problems_rdb': ''\n"
userConfig += "}\n"
if safeExecEnabled:
    userConfig += """\nUSE_SAFEEXEC = True
SAFEEXEC_USERID = """ + str(pwd.getpwnam(wsgiUser).pw_uid) + """
SAFEEXEC_GROUPID = """ + str(pwd.getpwnam(wsgiUser).pw_gid) + """
"""

production = input(
    "Enable production mode? Answering n will enable debugging messages. (Y/n): ")
if production.lower() == "n":
    userConfig += """
PRODUCTION = False
DEBUG = True
"""

if sqlEnabled:
    sqlDebug = input("Enable SQL debugging? (Y/n): ")
    if sqlDebug.lower() == "y" or sqlDebug == "":
        userConfig += """
SQL_DEBUG = True
"""
    studentDatabase = input("Enter the database for PCRS SQL: ")
    userConfig += """RDB_DATABASE = '""" + studentDatabase

print("Admin Information (Email Notifications)")
userConfig += "\nADMINS = ("
while True:
    adminName = input("Enter an Admin name (Blank to stop): ")
    if adminName == "":
        break
    adminEmail = input("Enter the Admin's email.")
    userConfig += "\n('" + adminName + "', '" + adminEmail + "'),"

userConfig += "\n)\nMANAGERS = ADMINS\n"

print("Database Information")
dbName = input("Enter the database name: ")
dbUser = input("Enter the database user: ")
dbPass = input("Enter the password for user " + dbUser + ": ")
dbHost = input("Enter the database host (Empty for localhost): ")
dbPort = input("Enter the database port (Empty for default): ")
userConfig += """
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '""" + dbName + """',
        'USER': '""" + dbUser + """',
        'PASSWORD': '""" + dbPass + """',
        'HOST': '""" + ("localhost" if dbHost == "" else dbHost) + """',
        'PORT': '""" + dbPort + """',
    }
}"""

# Site prefixing (We know this info already)
userConfig += """
\nSITE_PREFIX = '""" + prefix + """'
FORCE_SCRIPT_NAME = SITE_PREFIX
LOGIN_URL = SITE_PREFIX + '/login'"""

print("Select an authentication method:")
print("1) shibboleth")
print("2) pwauth")
print("3) pass")
print("4) none (*DANGEROUS*)")
authSelection = input("Selection: ")
authSelection = "shibboleth" if authSelection == "1" else "pwauth" if authSelection == "2" else "pass" if authSelection == "3" else "none"
userConfig += """
AUTH_TYPE = '""" + authSelection + """'
AUTOENROLL = False
"""

allowedHosts = []
print("Allowed hosts (Required in production)")
while True:
    host = input("Enter a host (Blank to stop): ")
    if host == "":
        break
    allowedHosts.append(host)

userConfig += """
ALLOWED_HOSTS = """ + repr(allowedHosts)

secretToken = secrets.token_urlsafe(37)
userConfig += """
SECRET_KEY = '""" + secretToken + """'

"""

tgtfile = f"pcrs/settings_local.py"
print(f"Writing config to \"{tgtfile}\"")
print("Writing Config...")
file = open(tgtfile, "w")
file.write(userConfig)
file.close()
print("Config Generated!")

print("""
Recall: before/after running this, make sure to setup virtualenv 
if desired and install any requirements with 
(sudo) pip3 install - r requirements.txt
""")
