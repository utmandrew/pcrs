# Make sure to build (w/ ./build.sh located in riscvview dir) before this.
rm -rf "static/riscvview"
cp -r "pcrs/languages/riscv/riscvview/build" "static/riscvview"